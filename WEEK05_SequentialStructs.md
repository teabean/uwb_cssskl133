# SEQUENTIAL STRUCTS

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1 - Operator Overloading](#task-1)
- [ ] [Task 2 - Hashing](#task-2)
- [ ] [Task 3 - Binary Representation](#task-3)
- [ ] [Task 4 - Recursion](#task-4)
- [ ] [Task 5 - Recursion](#task-5)
- [ ] [Task 6 - Binary Node](#task-6)
- [ ] [Pre-lab 1](#pre-lab-1)
- [ ] [Pre-lab 2](#pre-lab-2)

## ADMINISTRIVIA

### FOLLOWUPS
Deqeues - Just double-ended linked lists that allow all the push/pop operations to be performed from either the front or the back. Turns out files on your computer work this way, where the OS usually allows you tp open/write to a file from the front, the rear, or somewhere in the middle. Why might this be useful?

Real queues - Input and output devices almost all rely on queues. Network communications and routers also rely heavily on queueing for their operation.

Infix, Postfix, and Prefix notations
Differs from classical math statements in that parentheses are superfluous.
- Prefix Notation (Polish Notation) - Operators are written **before** their operands ( + X Y )
- Infix Notation - Operators are written **in-between** their operands ( X + Y )
- Postfix Notation (Reverse Polish) - Operators are written **after** their operands ( X Y + )

Prefix notation is actually direct function calls:
```+ X + Y Z```
Translates directly to `add( x, add( y, z ) )`. This made it handy for simple calculators

Parse Trees
- Gonna get back to this one next week

## EXIT CODES AND SHELL CONTROL FLOW
A number from 0 to 255 which is returned from a child process to a parent process.

> Surprise! Your program is actually pretty much the same as a function call! <br>
> Which maybe isn't surprising; it does start with a call to main().

### NUMERIC MEANING
There is none inherent, although industry standard in POSIX is `0` for success, `1-255` for everything else.

> 'Everything Else' depends on the application developer. The meaning must be documented and conveyed to the application users to be useful.

Similar to port numbering in networks, [there are recommendations](https://tldp.org/LDP/abs/html/exitcodes.html) for how to use the numbers, but this is not enforced or enforceable.

### ACCESSING THE EXIT CODES
Upon process closure, the exit code is stored to a variable `$?`

Short test program:

```
touch exit.test
cat exit.test
echo $?
```

To see `cat` failure code, attempt to cat something that doesn't exist

```
cat doesnot.exist
echo $?
```

#### INTERLUDE - $ BASH
The command `exit 123` causes the current process to exit with a code of '123', but executing it in your Bash shell actually causes the terminal to close. You can open a Bash shell -in your Bash shell- by using the command 'Bash'

```
bash
exit 123
echo $?
```

Doing so allows a command to be executed in the internal shell, the exit code of which is then returned to the shell that called it. Usually the Bash shell is handy for navigating to another location to perform a task, then "warping" back to where you were previously, but it can also be a powerful tool in shell scripting.

Shells may be opened in a new window using several installed tools that may not come packaged with your Ubuntu distribution:

```
gnome-terminal
konsole
xterm
```

#### BASH CONTROL FLOW - IF STATEMENTS
```
cat file.txt 

if [ $? -eq 0 ]
then
  echo "The script ran ok"
  exit 0
else
  echo "The script failed"
  exit 1
fi
```

## STREAMS
You can insert things to a stream and consume them in the order by which they were inserted.

Sound familiar?

A stream is a class of object, and a queue might be the data structure it's built with. The queue, in turn, might be built using a linked list. Data structures can support each other.

### STREAM OPERATOR >> AND <<
Many object classes have "operators". These are just special characters that are used as a stand-in for a function call.

```
myNumberClass n = 123 + 45.6;
```

Above, the `+` is the operator. What actually happens under the hood is that C++ will look for a particular function within the `myNumberClass` named `operator+`, whose arguments match the data types of the tokens to the left and right of the operator (in this case, an int and a float).

```
operator+( int x, float y ) {...}
```

If C++ finds this function, it places the left term (`123`) to the first argument, right term (`45.6`) to the second argument, then proceeds to do whatever is defined in `operator+( int, float )`.

So what does stream insertion `<<` and stream extraction `>>` actually do? Whatever is defined for the class in `operator>>` and `operator<<`. **Usually**, extraction consumes some portion of the stream and **usually** insertion appends material to the stream, but you'll have to consult the documentation of the class to know for sure.

### FRIEND
Your class can declare a target class as a `friend`. Doing so gives the target class access to private variables in much the same way that declaring a friend to your household might provide that person access that strangers would not have.

```
#include <iostream>

class MyDate {
private:
    int month;
    int day;
    int year;

public:
    // Just a constructor
    MyDate( int mm, int dd, int yy ) {
        month = mm;
        day = dd;
        year = yy;
    }

    // Declare the class
    friend std::ostream& operator<<( std::ostream& friendedStream, const MyDate& outputDate );
};

std::ostream& operator<<( std::ostream& friendedStream, const MyDate& outputDate ) {
    friendedStream << outputDate.year << '.' << outputDate.month << '.' << outputDate.day;
    return friendedStream;
}

int main() {
    MyDate testDate( 2021, 2, 4 );
    std::cout << testDate << std::endl;
}
```

Compile and run with:
```
g++ --std=c++17 main.cpp -o main.o
./main.o
```

An alternate approach using a toString() method:
```
#include <iostream>
#include <sstream>

class MyDate {
private:
  int year;
  int month;
  int day;

public:
  // Just a constructor
  MyDate( int yy, int mm, int dd ) {
    month = mm;
    day = dd;
    year = yy;
  }

  std::string toString( ) {
    // Because you can't naively append ints to std::strings
    std::ostringstream concatenator;
    concatenator << this->year << '.' << this->month << '.' << this->day;
    return concatenator.str();
  };
};

int main() {
  MyDate testDate( 2021, 2, 4 );
  std::cout << testDate.toString() << std::endl;
}
```

### TASK 1
> OPERATOR OVERLOADING<br>
> Modify the above code block to append an ".HH:MM:SS.SS" to the ostream output. <br>
> <br>
> [Back to checklist](#task-checklist)

## HASHING
Hash functions just convert one number to a fixed output. Let's look at the simplest one you could possibly make:

```
int hash( int n ) {
  return 0;
  // or return n - This is known as the "identity hash"
}
```

> Note that return( rand() ) is -not- a hash function because the output is not fixed. <br>
> return( rand(input) ), however, -is- a hash function. <br>

So what can we use this for? Let's suppose we use the identity hash and are able to allocate an array of size
9,999,999,999; we could then use this to store (for instance) phone numbers, and the array could be used as a starting point to find other information associated with the number in O(1) time. Handy!

![Hashing](Img/hashing.jpg)

This can be used for things like if you have a database of users, each with a unique ID. Systems like Social Security or customer databases lend themselves pretty well to this strategy, allowing immediate lookups based off the ID number.

However, if you number off sequentially, that also means a lot of entries on the identity hash table aren't going to be used for a very long time. This isn't really a space-efficient lookup strategy. If we have 10,000 customers, we would ideally like for the storage structure to be about the same size.

One possible solution is to just modulo divide the identity by the table size.

```
indexPosition = hash % tableSize;
```

However, because the table is smaller than the possible hash inputs, the hash outputs will on occassion be the same for multiple input values. This is known as a **collision**. There are strategies for dealing with them, but the first goal is to just have fewer. The most common strategy is to leave it to smart math peoples and just use their pseudorandom functions for this:

```
indexPosition = rand( input ) % tableSize;
```

So that's all quite useful!

> Hash tables are one of the most consistent ways to ensure O(1) lookups in an array of values.

We can go a bit further and formalize what we're looking for in a "good" hash function.

<dl>
<dt>Consistent</dt>
<dd>The same inputs should always map to the same outputs.</dd>

<dt>Fixed Range</dt>
<dd>The output is a number within a set range.</dd>

<dt>Even Distribution</dt>
<dd>Outputs within the range should be statistically randomized</dd>

<dt>One-Way</dt>
<dd>This is security related, but the hash should not provide information about the input that created it</dd>
</dl>

It's a fingerprint of the input, basically.

### HASHING OF PASSWORDS
When you provide your password to a network service, the service can't retain the password. If the database itself gets stolen (and it will), then all the users' passwords would go with it.

So what your browser should do is hash the password, then send -that- to the service. The service can then check to see if the hash matches and, if it does, the service knows the password was entered correctly without ever knowing what the password actually was.

#### SIZE OF PASSWORD HASHES
The output result of this function (the function is SHA-256 or SHA-512 if you wanna look it up) is either 256 to 512 bits. The number of possible hashes, then, is astronomically large. Attempting to brute-force a 512 bit hash will take a modern computer something on the order of 10^64 years. Basically we can't really find a password from a hash.

#### RAINBOW TABLES
One possible approach to "cracking" the password hash problem is to pre-compute all the possible hashes that could be developed from all possible password inputs. Supposing if we had a short password of just one character, each character returns a hash in a very large space. However, we could use the hashes themselves to derive an index in a hash table, then, if any of those passwords happened to turn up in the future, we can use the table to determine what the original password was in O(1) time.

This strategy works on passwords up to about 9 characters in length, after that point modern drives just don't have enough space to hold the pointer references needed to make larger tables.

However, attackers got a little smarter and instead of pre-hashing -all- the passwords, they selected the top 10^14 or so most popular passwords in what's called a 'Dictionary Attack". This allowed them to reach a bit further beyond the previous 9 character limits.

#### DEFENSES AGAINST RAINBOW TABLES
The most obvious two solutions are:
1) Force users to select "odd" passwords
2) Force users to select longer passwords

From a usablity standpoint, neither of these options is ideal. Users generally don't want to remember a 30-character long string of gobbledegook.

The system itself can enforce these protections automatically, however, by applying something called a **salt**. This is just a user-specific constant that can be appended to their password that can force the hash input into a range safely outside the rainbow table range.

### TASK 2
> HASHING <br>
> Assuming you have a hash table of 256 elements, at what index does the password "Password123" hash to?
> Use an online SHA-512 generator: https://emn178.github.io/online-tools/sha512.html <br>
> - Read the last two digits of the code, this corresponds to the last byte of information (8 bits)
> - Convert this to decimal
> - That's your index! <br>
> <br>
> [Back to checklist](#task-checklist)

### TASK 3
> BINARY REPRESENTATION <br>
> Why is it that you can ignore all the rest of the hash output and just focus on the last byte? <br>
> Would this shortcut still work if your hash table were, say, 200 elements in length? <br>
> <br>
> [Back to checklist](#task-checklist)

## RECURSION
Recursion can be a tricky topic. What is it? Recursion can be a tricky topic. What is it? Recursion...

But seriously, recursive functions are like any other subroutine; the only trippy thing about it is that they (at some point) call themselves. Let's look at the simplest recursive function:

```
recur() {
  recur();
}
```

People tend to freeze up, but consider the following:

```
recur() {
  someOtherFunction();
}
```

In the above case, all that happens is that execution will transfer to someOtherFunction(), then proceed until complete. When complete, the execution pointer finished the function call and proceeds to the end. Recursive calls do the same thing, just the function definition happens to be the same as the definition which called it.

The immediate problem, of course, is that a self-referential call will go on forever or until your call stack is exhausted and the OS tells you to stop. To prevent this, we might interject a return before the calls have a chance to run away forever.

```
recur() {
  return;
  recur();
}
```

Of course, now the function cannot recur at all, since it hits the return and then stops. We can avoid this by placing the return inside of a test:

```
recur() {
  if( someConditionIsTrue ) {
    return;
  }
  // Otherwise...
  recur();
}
```

This is the basic structure of a working recursive function. The conditional and its return is known as the **base case**, since that represents the lowest level of recursion we can reach before the call stack begins to collapse back upwards. From here, there's an almost endless number of embellishments we can add, but they generally have to be structured around the basic recursive form defined above:

```
returnType recur( /* You can do things with THE ARGUMENTS */ ) {
  // You can do things BEFORE THE BASE CASE
  if( someConditionIsTrue ) {
    // You can do things DURING THE BASE CASE
    return( /* You can do things with the BASE CASE RETURN */ );
  }
  // You can do things BEFORE THE RECURSIVE CALL
  recur( /* You can do things WITH THE RECURSIVE CALL */ );
  // You can do things AFTER THE RECURSIVE CALL
  return( /* You can do things with the RECURSIVE RETURN */ )
}
```

### TASK 4
> RECURSION <br>
> Write a recursive function that starts a counter at 20, decrements the counter by 1 for every recursive call, and halts at 1. Print a success message to console in the base case. <br>
> <br>
> [Back to checklist](#task-checklist)

### TASK 5
> RECURSION... AGAIN. <br>
> Modify the above function such that the base case result is returned to the prior recursive call, multiplied by 2 on every return. Print a success message to console after all recursive calls are complete. <br>
> <br>
> [Back to checklist](#task-checklist)

## PARENT-CHILD NODE RELATIONSHIPS
In the linked list examples from last week, every node made reference to just one other node (or sometimes the nodes were back-linked, as in doubly-linked lists). This is sometimes also called a parent-child relationship, where the parent is the node that references another, and the child is the one that is referenced.

It should be noted that there isn't actually any limit to the number of child nodes a parent can have (or vice versa). You can make a Node class that can point to 20 or a hundred (or any number) of child nodes. Doing so moves us into a realm of study called [Graph Theory](https://en.wikipedia.org/wiki/Graph_theory). The field started as a mathematical curiosity in the 1930s, but has since found application in pretty much all domains that involve pairwise relationships between objects (and that's, like... all of the domains). Some familiar examples include your phone's navigation system and the internet as a whole (in fact, movement of data across the internet would be nigh impossible without the usage of graph theory)

### BINARY TREES
A particularly important subset of graphs are the **binary trees**. In a binary tree, certain rules must be followed:

- The tree begins at a node
- Every parent has at most **2 children**

![Binary tree](Img/bintree.jpg)

There are also a few conventions:
- The first node is called the **root**
- The two children are called either **Left** or **Right**
- Generally, there are no **loops** nor shared relationships
- Moving across a tree is called a **traversal**

### TASK 6
> BINARY NODE <br>
> Define a class `Node` which represent a node in a binary tree. <br>
> It should contain a value and two Node pointers labeled 'left' and 'right' <br>
> <br>
> Alongside this, define a class `BinaryTree`, noting what the class must have (its fields) and what it must be able to do (its functions)<br>
> [Back to checklist](#task-checklist)

## PRELAB 1
> Draw the 3 binary trees A, B, and C using the following rules: <br>
> The left child's values must be higher than the parent's. <br>
> The right child's values must be less than the parent's. <br>
> <br>
> Where: <br>
>   A) The values are randomly selected <br>
>   B) The values are monotonically ascending <br>
>   C) The values are monotonically descending <br>
> <br>
> [Back to checklist](#task-checklist)

## PRELAB 2
> Trees B and C are known as 'degenerate trees'. What do you suppose these binary trees are "degenerating" into? <br>
> <br>
> [Back to checklist](#task-checklist)
