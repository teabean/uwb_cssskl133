#include <stdio.h>
#include <stdlib.h>

void populate( int* theArray, int count );
void printArray( int* theArray, int count );
void sortArray( int* theArray, int count );
void swap( int* a, int* b );
int partition (int arr[], int low, int high);
void quickSort(int arr[], int low, int high);

int operations = 0;

int main( int argc, char* argv[] ) {
  // argc is 2 (program name + argument you provide)
  // argv[0] is main.out (or whatever the executable name is)
  // argv[1] is "123"
  char *theArg = argv[1];
  int caughtNumber = atoi( theArg );
  printf( "%d \n", caughtNumber );
  
  int theArray[ caughtNumber ];
  populate( theArray, caughtNumber );

  printArray( theArray, caughtNumber );

  printf( "\nSorting... \n\n");

  sortArray( theArray, caughtNumber );

  printArray( theArray, caughtNumber );
  printf( "Operations: %d \n", operations );

}

/*
void sortArray( int* theArray, int count ) {
  for( int i = 0 ; i < (count-1) ; i++ ) {
  	for( int j = count ; j > 1 ; j-- ) {
      operations++;
      if( theArray[ count - j ] > theArray[ ( count - j ) + 1 ] ){
    	swap( &theArray[ count - j ], &theArray[ ( count - j ) + 1 ] );
  	  }
  	}
  }
}
*/

void sortArray( int* theArray, int count ) {
  quickSort( theArray, 0, count-1 );
}

int partition (int arr[], int low, int high) { 
    int pivot = arr[high];    // pivot 
    int i = (low - 1);  // Index of smaller element 
  
    for (int j = low; j <= high- 1; j++) { 
        // If current element is smaller than the pivot 
        operations++;
        if (arr[j] < pivot) { 
            i++;    // increment index of smaller element 
            swap(&arr[i], &arr[j]); 
        } 
    } 
    swap(&arr[i + 1], &arr[high]); 
    return (i + 1); 
}

void quickSort(int arr[], int low, int high) { 
	operations++;
    if (low < high) { 
        /* pi is partitioning index, arr[p] is now 
           at right place */
        int pi = partition(arr, low, high); 
  
        // Separately sort elements before 
        // partition and after partition 
        quickSort(arr, low, pi - 1); 
        quickSort(arr, pi + 1, high); 
    } 
}

void swap( int* a, int* b ) {
  operations++;
  int temp = *b;
  *b = *a;
  *a = temp;
}

void populate( int* theArray, int count ) {
  for( int i = 0 ; i < count ; i++ ) {
  	theArray[ i ] = rand() % 100;
  }
}

void printArray( int* theArray, int count ) {
  for( int i = 0 ; i < count ; i++ ) {
  	printf( "%d ", theArray[i] );
  }
  printf( "\n" );
}
