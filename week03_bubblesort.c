#include <stdio.h>
#include <stdlib.h>

void populate( int* theArray, int count );
void printArray( int* theArray, int count );
void sortArray( int* theArray, int count );
void swap( int* a, int* b );

int operations = 0;

int main( int argc, char* argv[] ) {
  // argc is 2 (program name + argument you provide)
  // argv[0] is main.out (or whatever the executable name is)
  // argv[1] is "123"
  char *theArg = argv[1];
  int caughtNumber = atoi( theArg );
  printf( "%d \n", caughtNumber );
  
  int theArray[ caughtNumber ];
  populate( theArray, caughtNumber );

  printArray( theArray, caughtNumber );

  printf( "\nSorting... \n\n");

  sortArray( theArray, caughtNumber );

  printArray( theArray, caughtNumber );
  printf( "Operations: %d \n", operations );

}

void sortArray( int* theArray, int count ) {
  for( int i = 0 ; i < (count-1) ; i++ ) {
  	for( int j = count ; j > 1 ; j-- ) {
      operations++;
      if( theArray[ count - j ] > theArray[ ( count - j ) + 1 ] ){
    	swap( &theArray[ count - j ], &theArray[ ( count - j ) + 1 ] );

  	  }

  	}
  }
}

void swap( int* a, int* b ) {
  operations++;
  int temp = *b;
  *b = *a;
  *a = temp;
}

void populate( int* theArray, int count ) {
  for( int i = 0 ; i < count ; i++ ) {
  	theArray[ i ] = rand() % 100;
  }
}

void printArray( int* theArray, int count ) {
  for( int i = 0 ; i < count ; i++ ) {
  	printf( "%d ", theArray[i] );
  }
  printf( "\n" );
}
