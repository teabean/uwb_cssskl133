# OBJECTS AND CLASSES

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1 - Design Your Own Class](#task-1)
- [ ] [Task 2 - Classes, Superclasses, Subclasses](#task-2)
- [ ] [Task 3 - Chonky-Boi](#task-3)
- [ ] [Task 4 - The Nature of Static](#task-4)
- [ ] [Task 5 - Linked Lists, Stacks, and Queues](#task-5)
- [ ] [Pre-lab 1](#pre-lab-1)
- [ ] [Pre-lab 2](#pre-lab-2)

## ADMINISTRIVIA

### FOLLOW-UPS

#### QUICKSORT
[Quicksort explanation](quicksort.md)

## CLASS OBJECTS: AN OVERVIEW
What is a class? Let's start by defining a class of object called the 'MP3 Player', which should describe the aspects and capabilities of... well... MP3 players. From experience, you should have a pretty good idea of what these things do, but let's formalize that into two lists; what players have (their properties) and what they do (their capabilities)

MP3 players have/are:
- Weight
- Color
- Memory capacity
- Battery life
- Price
- Name
- Model
- Manufacturer
- Etc.

MP3 players can:
- Store songs
- Play songs
- Skip forward
- Skip back
- Download
- Upload
- Charge
- Etc.

Broadly, these descriptive lists can be translated directly into what we might describe as **fields** (or properties) and **methods/functions** (capabilities).

| MP3 PLAYER |
| :--- |
|Weight <br>Volume <br>Color <br>BatterySize <br>BatteryQuantity <br> AudioJackFormat <br> CanRecord <br> HasRadio <br> CostInCents <br>       IsULListed |
|playSong() <br>rewindSong() <br>fastForwardSong() <br>recordCassette() <br>changeBatteries() <br>changeVolume() |

### TASK 1
> DESIGN YOUR OWN CLASS <br>
> Select a class of object other than the MP3 player and describe the fields and methods that this class of object might possess. Format your response as two lists, the first being the fields of the class, the second being the functions.<br>
> <br>
> [Back to checklist](#task-checklist)

Congratulations! You just created a rudimentary [class diagram](https://en.wikipedia.org/wiki/Class_diagram)! In a software context, the class diagram is a succinct description of what a logical "object" is and can do. When you put multiple class diagrams together and start describing how they connect, you end up with a [UML diagram](https://en.wikipedia.org/wiki/Unified_Modeling_Language).

These diagrams were developed in the mid 90s to try and standardize the (at the time) many disparate notation methods for representing and communicating data flow and component relationships. As you might imagine, this can become a complicated undertaking on larger projects.

Next, let's consider briefly if the class you selected was derived from a broader class of object (hint: yes, it was) and - in the inverse - what are the subclasses to your selection (for instance, MP3 players might divide into software MP3 players, embedded MP3 players, players over $100, etc.).

### TASK 2
> SUPERCLASSES, SUBCLASSES, OTHERCLASSES <br>
> Provide examples of <br>
> - A few superclasses <br>
> - A few subclasses <br>
> <br>
> Lastly, also consider if your decisions regarding how the super- and sub-classes were divided was the best or only way to perform this division. Would it have made sense (though perhaps not necessarily more sense) to generate classe divisions by color, manufacturer, date of release, or some other method? List a few other ways you might distinguish super- and sub-classes. <br>
> <br>
> - A few 'other' class divisions <br>
> <br>
> [Back to checklist](#task-checklist)

This thought exercise touches on the principles of **inheritance** (which you'll get to presently). For now, just know that this is a larger problem related to a field called [cladistics](https://en.wikipedia.org/wiki/Cladistics). Indeed, the problem of how we classified plants and animals (prior to DNA testing and the evolutionary model of life, that is) shares many parallels with how to effectively engage with classes and inheritance.

##### FOOD FOR THOUGHT:
In order to fully define what a human being (homo sapiens) is, we really only need to stipulate that they are a member of the genus 'homo', plus a handful of distinctions specific to H. sapiens.

![Homo Sapiens](Img/homo_sapiens.jpg)

We do -not- need to explicitly describe features like possession of a spine, red blood cells, binocular vision, or the fact that we are deuterostomes.

> How might abbreviating the description of species be applicable to logical classes of objects? <br>
> <br>
> Hint: If your wrote Homo Sapiens as Animalia.Chordata.Mammalia.Primates.Haplorhini.Simliformes.Hominidae.Homininae.Hominini.Homo.HomoSapiens you would fully describe the descent of the species, along with ALL of its fields and methods.

### DECLARING A CLASS
Similar to functions and variables, we have to declare a class before we're able to use it in a program. Let's start with a simple class called 'Dog'. Java first requires that we create a file called Dog.java (the file name and class name must be in agreement and are both case-sensitive).
Copy the below code and compile. Your output should be blank (nothing happens, but at least there are no compiler errors!).

`main.cpp`
```cpp
#include "doge.cpp"

int main() {
  Dog* rover = new Dog();
  return 0;
}
```

`doge.cpp`
```cpp
class Dog {
  // Classes are 'private' by default
};
```
Compile and run with:
```
g++ --std=c++17 main.cpp -o dog.out
./dog.out
```

Ignoring for the moment what the 'Dog' class will do or be, we have confirmed that we are able to **instantiate** an instance of this class.

**Whoop! Hold up! What's this 'instantiation' mean?**

Consider for a moment the Platonic ideal of an object. For example, the "dog". As a class of objects, dogs have certain traits, capabilities, and shared properties that allow us to identify a thing as a dog.

![The Platonic Dog](Img/dog_platonicideal.jpg)

`class dog{ };` <br>
Probably fetches. Is good. Woofers.

However, this Platonic idea of a "dog" does not exist in the real world. Does "dog" have a color, weight, or name? Can it fetch a stick or be the best, fluffiest, who's a good puppy? Yes! You're the… 

*Ahem*

No, the class "dog" is a useful template of an object, but we can only interact with instances of dogs. These are real dogs who (to bring it back to software) may be defined by the 'dog' class definition, but are separate and apart from it.

dog KaiserElCidXIII;

![A Dog Instance](Img/dog_kaiserelcidXIII.jpg)

More photogenic than I will ever be

**Back to the Dog class**

Next, let's add a little bit of functionality to the Dog class by adding a few fields. Fields are object variables; you may think of them as parameters or qualities of an object:

`main.cpp`
```cpp
#include <iostream>
#include <string>
#include "doge.cpp"

int main() {
  Dog* rover = new Dog();
  return 0;
}
```

`doge.cpp`
```cpp
#include <iostream>
#include <string>
class Dog {
  std::string name   = "Rover"; // <---------------!
  std::string hooman = "Foo Bar Baz"; // <---------------!
  std::string color  = "Doggo"; // <---------------!
  int numLegs   =  4; // <---------------!
  int weight_kg = 15; // <---------------!
};
```
Compile and run with:
```
g++ --std=c++17 main.cpp -o dog.out
./dog.out
```

### DEREFERENCE, ARROW, AND DOT NOTATION
Usually, to interact with these fields, we have to call them in a fashion similar to how we interact with other variables. You can kind of think of the creation of 'rover' as being the bulk creation of all the fields defined in the object.

However, that means that all Dog objects share fields with the same name. In order for the compiler to select the correct field from the correct object, we need to use something called **dot notation** to define which Dog object we want, **then** which field in the object we're asking for.

`main.cpp`
```cpp
#include <iostream>
#include <string>
#include "doge.cpp"

int main() {
  Dog* rover = new Dog();
  rover.name; // <---------------!
  return 0;
}
```

However, attempting to compile this will result in an error, because `rover` is not actually an object: It is an object **pointer**. Being a pointer, we have to dereference is, **then** use the dot notation.

`std::string myDogsName = *rover.name;`

However, this will also throw an error because C++ attempts to resolve `(rover.name)` first, then the dereference. While what we're trying to express is logically correct, we have to coerce the order of operations using parentheses.

`std::string myDogsName = (*rover).name;`

This is all getting a bit overwrought, however, so the developers of the language specified a shortcut, the `->`

`std::string myDogsName = rover->name;`

I prefer to pronounce this as 'dereference then access', but the official name is the `->` operator.

`main.cpp`
```cpp
#include <iostream>
#include <string>
#include "doge.cpp"

int main() {
  Dog* rover = new Dog();
  rover->name; // <---------------!
  return 0;
}
```

And if you compile, again, nothing seems to happen. The call to `doge.name` returns an element of data type `std::string`. As with any `std::string`, in order to do something with it, we have to either assign it or process it immediately (as to `std::cout`).

`main.cpp`
```cpp
#include <iostream>
#include <string>
#include "doge.cpp"

int main() {
  Dog* rover = new Dog();
  std::string myDogsName = rover->name; // <---------------!
  // OR
  std::cout << ( rover->name ); // <---------------!
  return 0;
}
```

Success!

### TASK 3
![Chonkers](Img/chonkyboi.jpg)

>Field assignment works in the reverse fashion. Suppose you feed Rover a bunch of treats and he gains 2 kilos in weight, then you wish to rename him to 'Chonkyboi'. <br>
> <br>
> Complete the following code block to rename and reassign the weight of Rov- er, Chonkyboi.

```cpp
#include <iostream>
#include <string>
#include "doge.cpp"

int main() {
  Dog* myDoge = new Dog();
  <TODO - Add 2 kilograms to weight_kg>;
  <TODO - Change name to 'Chonkyboi'>  ;
  std::cout << "My dog is named: " << myDoge->name << std::endl;
  std::cout << "His weight is  : " << (myDoge->weight_kg/6.35) << " stones" << std::endl;
}
```
> <br>
> [Back to checklist](#task-checklist)

### STATIC
![Sled Team](Img/sledteam.jpg)

Let's suppose that you're lucky enough to acquire multiple dogs.

`main.cpp`
```cpp
#include <iostream>
#include <string>
#include "doge.cpp"

int main() {
  Dog* rover1 = new Dog();
  Dog* rover2 = new Dog();
  Dog* rover3 = new Dog();
  Dog* rover4 = new Dog();
  std::cout << ( rover1->hooman ) << std::endl;
  std::cout << ( rover2->hooman ) << std::endl;
  std::cout << ( rover3->hooman ) << std::endl;
  std::cout << ( rover4->hooman ) << std::endl;
  return 0;
}
```

`doge.cpp`
```cpp
#include <iostream>
#include <string>

class Dog {
  public:
  std::string name   = "Rover";
  std::string hooman = "Foo Bar Baz";
  std::string color  = "Doggo";
  int numLegs   =  4;
  int weight_kg = 15;
};
```

Unfortunately, they've all been mis-registered (Dog.hooman) to somebody named 'Foo Bar Baz'! In order to remedy this, you could go to each Dog and reassign the Dog.hooman value...

However, if you know that all the dogs in a program will share the same owner, you can also make the 'hooman' variable static, then assign the value just once either to an instance of the Dog class or to the Dog class in general.

`doge.cpp`
```cpp
#include <iostream>
#include <string>

class Dog {
  public:
  static std::string hooman; // The class only declares where the storage element will go
  std::string name   = "Rover";
  std::string color  = "Doggo";
  int numLegs   =  4;
  int weight_kg = 15;

};

std::string Dog::hooman = "Foobarbaz"; // But the storage element has to be created here
```

`main.cpp`
```cpp
#include <iostream>
#include <string>
#include "doge.cpp"

int main() {
  Dog* rover1 = new Dog();
  Dog* rover2 = new Dog();
  Dog* rover3 = new Dog();
  Dog* rover4 = new Dog();

  rover1->hooman = "YOUR NAME GOES HERE";

  std::cout << ( rover1->hooman ) << std::endl;
  std::cout << ( rover2->hooman ) << std::endl;
  std::cout << ( rover3->hooman ) << std::endl;
  std::cout << ( rover4->hooman ) << std::endl;
  return 0;
}
```

The keyword static makes one variable (or function, if used in that context) that is shared between all instances of a class. Hence, you can reference static elements via any instance, or even no instance at all! This is similar to how you can call 'Math.add()' without having to instantiate any sort of 'Math' object.

**A WORD OF CAUTION**
'Static' elements can become extremely dangerous in the context of multi-threaded programs. Imagine, if you will, that I provide the entire class a program to execute (you're all parallel processors!). But then I say that there's a special rule: you all have to use the same blackboard to store your data and show your work.

![Whiteboard](Img/whiteboard.jpg)

By the time all of you are midway through, there's likely to be very little confidence left in the values on the board. Who changed what and when? We have no way of knowing; only the final messy state of the board will be left. This is a group of problems known as race conditions, wherein the order and access to data elements must be carefully controlled in order for the correctness of output to be guaranteed.

### TASK 4
> Determine whether the following object fields/methods of the 'Dog' class  can be appropriately static or not:
```
float  heartRate    - <TODO - Static or not>
String genus        - <TODO - Static or not>
Stick  fetch()      - <TODO - Static or not>
String mood         - <TODO - Static or not>
boolean likesTreats - <TODO - Static or not>
Wolf mostRecentCommonAncestor - <TODO - Static or not>
```
> <br>
> [Back to checklist](#task-checklist)

### PUBLIC / PRIVATE
![Pickles](Img/pickles.jpg)

To a first level approximation, you may think of class instances as jars or containers. Every time you make a class object, you create a new jar and this jar is filled with data according to its class definition. However, these jars are able to hold information without disclosing it; a condition known as 'private'. In the real world, you might imagine that certain elements of a pickle jar can be discerned from external examination (public), while others are known only to the jar (private):

| PUBLIC | PRIVATE |
| :--- | :--- |
| JarSize <br> JarColor <br> JarWeight <br> NutritionInfo <br> Manufacturer <br> PickleType <br> Quantity | ActualPickleType <br> Flavor <br> Texture <br> AccurateCount <br> ContentWeight<br> |

You may note that between the two categories, the information which is public is usually related to the information that is private.

#### PUBLIC: / PRIVATE: TAGS
The `public:` and `private:` tags are used within a class declaration in order to specify what fields or methods are, well, public or private.

`picklejar.cpp`
```cpp
#include <iostream>
#include <string>

class PickleJar {
  private: // Note that classes are private by default, so this is a formality
  std::string flavor;

  public:
  int weight;
};
```

### GETTERS / SETTERS
But wait, you say, what's the point of putting information in this class if - like a real pickle jar - you can never get it back out? While it's true that other modules of code won't be able to access these fields, what's important is that the object -itself- has access, and may divulge information based on its own discretion.

The most common of these are a pair of functions we call **getters** and **setters**. Getters are used to return the value of a private field without sending back the field itself, while setters are used to set a private field. Often, a getter/setter pair is generated for every private field you might want to access or edit.

But wait... if that's the case...

#### WTF IS THE POINT?
Admittedly, if you make a getter/setter that does nothing but get or set a value (as your initial assignments and works will require), you may as well just make the field public and avoid the pain of writing getters and setters. But what if the object is actually holding something sensitive? Consider the following:

A `Distance` object holds a distance measurement and is used to manage unit conversions. In order to store a value, you make a call as follows:

`myDistance.setDistance( 123, "inches" )`

To get a distance, you make a call as follows:

`myDistance.getDistance( "kilometers" )`

Based only on the description, you have a pretty good idea how to use the distance class and what your inputs and returns are going to look like. More than that, the setters and getters prevent you from making unit conversion errors, since you have to specify the unit format of both the input and the output.

And here's the interesting thing about private fields and methods; What is the underlying unit and how are the conversions handled?

The kicker is that as users of the class, **we don't care**. The internal implementation is up to the class to carry out, and the class itself ensures that all efforts to do squirrely things with the data are rejected. Setting these values to private - even if we're given ways to interact with them after the fact - enforces a black box abstraction that prevents us from improper (not just unauthorized) access.

> This sort of idea really comes to a head with something like a `Password` class. If I pass a String into the class, I really really really don't want any other part of the program to be able to look inside and see how the Password class is handling -any of its data-. At the same time, if I query the class with, say, a similar credential piece and ask for a comparison, I would like it if the underlying details were private but I could just receive a public "Yea/Nay" in response.

### CONSTRUCTORS
Every time an object is created, the object has to call a **constructor**. This is a method (a subroutine) that runs at the time of object creation. The simplest way to create a constructor is not to define or implement it at all, in which case your program will interject an **implicit** default constructor.

The default constructor kind of sucks, though, and the C++ specification doesn't really guarantee any behaviors except that your object and its associated fields will be declared in memory.

> Why is this insanely dangerous?

To define your own constructor, you need to declare it like other functions. The only difference is that the name of the function is the same as the name of the class.

Specifying different argument datatypes in differing orders creates different signatures, so you can have pretty much as many different constructors as you could desire (they all just need unique signatures).

`picklejar.cpp`
```cpp
#include <iostream>
#include <string>

class PickleJar {
  private: // Note that classes are private by default, so this is a formality
  std::string flavor;

  public:
  PickleJar() {
    // Whatever you want to have happen in the default constructor goes here
  }

  PickleJar( int arg1, char arg2, std::string arg3 ) {
    // Whatever you want to have happen in the constructor by int-char-string goes here
  }

  int weight;
};
```

> If all your constructors need to initialize the fields of an object, why not pack it into a singular function? A `tare()` function can at least ensure that all your fields are zeroed out, and by including it in every constructor, you can avoid having to update multiple constructors should you class definition change. Just change the `tare()` function, and the behavior updates in all constructors that used it!

#### DESTRUCTORS
On the flipside, whenever an object goes out of scope or is `deleted`, the destructor for the class gets called. Not all languages give you the ability to define a destructor, but C++ is one of them.

Unlike constructors, there is only one destructor.

`picklejar.cpp`
```cpp
#include <iostream>
#include <string>

class PickleJar {
  ~PickleJar() {
    // Whatever you want to have happen in the default constructor goes here
  }
};
```

> It is considered good practice to clear all your fields when destructing an object. The `tare()` function from the constructor can actually serve double-duty in this regard! <br>
> <br>
> What is the security hazard of -not- clearing your fields?

## CLASS OBJECTS APPLIED - THE NODE CLASS
So... stacks and queues. It's possible to implement them using an array as the underlying data storage mechanism, but this is less than ideal since we don't really know how big the stack or queue is going to be ahead of time, and re-allocating when we run out of space is sort of a hassle. It's doable, sure, but it might be preferred if we had a way of making the stack or queue exactly as big as it needed to be.

The other common storage mechanism is a Linked List, which you developed at the end of last quarter (and which we'll be revisiting now).

> In an actual, production setting, you'll want to use one of the standard library Stack or Queue data structures that comes with C++. These bodies of code have been far more thoroughly designed and tested than anything an individual programmer (or programming team, even) might realistically hope to do.

### THE LINKED LIST CLASS
Run the following code to generate a linked list:

```cpp
#include <iostream>

class Node {
  public:
  int value;
  Node* next;

  // Default constructor
  Node() {
    this->value = 0; 
  }
  Node( int arg ) {
    this->value = arg;
  }
};

class LinkedList {
  public:
  Node* head;

  LinkedList() {
    this->head = new Node( 0 );
  }

  void addToBack( int n ) {
    Node* curr = head;
    while( curr->next != nullptr ) {
      curr = curr->next;
      // We have moved to the tail of the list
    }
    curr->next = new Node( n );
  }

  void addToFront( int n ) {
    Node* temp = new Node( n );
    temp->next = this->head;
    this->head = temp;
  }

  void removeFirst() {
    // TODO: Check to see if there's anything after
    this->head = head->next;
  }

  void removeLast() {
    Node* curr = head;
    while( curr->next->next != nullptr ) {
      curr = curr->next;
    }
    delete curr->next;
    curr->next = nullptr;
  }

  void printList( ){
    Node* curr = head;
    while( curr != nullptr ) {
      std::cout << curr->value << " ";
      curr = curr->next;
    }
  }
};

int main( int argc, char* argv[] ) {
  LinkedList L = LinkedList();
  L.head->value = 123;
  std::cout << L.head->value << std::endl;
} // Closing main()
```

Compile and run with:
```
g++ --std=c++17 main.cpp -o main.o
./main.o
```

### TASK 5
> Consider the relationship between the Linked List class and a Stack or Queue. Provide your thoughts on what you would need to do to convert the Linked List above to: <br>
> 1) A Stack which can: <br>
>  A) Add to the 'top' in constant-time (`O(1)`) <br>
>  B) Remove from the 'top' in constant-time (`O(1)`) <br>
>  C) Provide a count of the elements in constant-time (`O(1)`) <br>
> 2) A Queue which can: <br>
>  A) Add to the 'back' in constant-time (`O(1)`) <br>
>  B) Remove from the 'front' in constant-time (`O(1)`) <br>
>  C) Provide a count of the elements in constant-time (`O(1)`) <br>
> 3) Can you perform all of the operations above in a single data structure in `O(1)`? <br>
> <br>
> [Back to checklist](#task-checklist)

## PRE-LAB 1
> A 'stack' is a construct that you're probably pretty familiar with; we have stacks of papers, stacks of trays, stacks of books, and so on and so forth. You're probably becoming familiar with the stack data structure, wherein we place data for storage on the 'top' of the stack, and consume that data from the 'top' (called a First-In-First-Out or 'FIFO' ordering). <br>
> There exists a structure in all computers that we refer to as the 'Call Stack'. It's also termed the 'execution stack', 'program stack', and 'control stack' (and probably a few more names besides; they're all the same thing). What do you suppose the call stack might be? <br>
> <br>
> [Back to checklist](#task-checklist)

## PRE-LAB 2
> Consider your real-world environment. Provide an example of a device you use which is likely utilizing a queue for its underlying data management and provide a rationale for why you believe this to be the case. <br>
> <br>
> [Back to checklist](#task-checklist)


