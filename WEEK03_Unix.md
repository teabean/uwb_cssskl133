# LINUX

# WEEK 3 - UNIX / LINUX

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1 - Unzipping Arrays](#task-1)
- [ ] [Task 2 - Lowercase](#task-2)
- [ ] [Task 3 - Command-Line Arguments](#task-3)
- [ ] [Task 4 - Random Array Population](#task-4)
- [ ] [Task 5 - Bubblesort](#task-5)
- [ ] [Pre-lab 1](#pre-lab-1)
- [ ] [Pre-lab 2](#pre-lab-2)

## ADMINISTRIVIA

### FOLLOW-UPS
#### USEFUL LINUX / UNIX COMMANDS
Thanks for adding to the spreadsheet! Here's a partial listing:

https://docs.google.com/spreadsheets/d/1N0N6wwtPMQlsX2sBSo4w3E0Z5te5IKJwr4nEICRn-3U/edit?usp=sharing

#### TEXT-BASED PROBLEMS
Almost everything computers are used for is, in a very real sense, a problem of text manipulation. That being said, what problems are computers applied towards? Practically everything in the modern world uses it to some extent or another.

## UNIX - A BRIEF HISTORY
- UNIX development starts in 1969 at Bell Laboratories in NJ
  - Multi-user, time-sharing operating system
  - Tried 'Multics' - project withdrawn
  - Team moved on to UNICS (UNiplexed Information and Computing Service)
- B language leads to C language (there's actually A through F languages)
- Unix kernel written in 1973
  - Released in 1975
  - Unix V6; free and open source
- UC Berkeley begines work on its own Unix version (Unix BSD)
  - Berkeley Software Distribution (BSD) - also a popular modern license
- 1980s: Everybody and their grandmother produces a version of Unix

## LINUX - A BRIEF HISTORY
- Development started in 1990
  - Linus Torvalds + Free Software Foundation (FSF)
  - Attempting to make an OS like Unix
- Version 1.0 released in 1994
- Richard Stallman + FSF create utilities for Linux
- By their powers combined: GNU/Linux!
  - But people just called in Linux
  - Linux is the kernel
  - GNU are the utilities
- Focus on simplicity, reliability

## PROBLEM 3.14 - UNZIPPING ARRAYS
Write a function that "unzips" an array into two half-sized arrays.

Array operations you might need:
```cpp
int array[] = { 1, 2, 3 }; // Make an int array of size 3, containing the values 1, 2, and 3
int array[ 10 ]; // Make an int array of size 10
functionCall( array ); // Pass an array

void functionCall( int* array ) {  // Catch an array
  array[0] = 12345; // Access caught array
  sizeof( array ) / sizeof( int ); // Count the elements in the array (ish, doesn't really work)
}
```

### TASK 1
> In plain English, explain how your unzipping program should operate (or if you've already done it, how it operates). <br>
> [Back to checklist](#checklist)

```cpp
// In pseudocode
// Function declaration for unzip()

// Establish the array
// Assume that it is evenly sized

// Establish smaller array 1

// Establish smaller array 2

// Perform the function call to unzip
unzip()

// Define the unzip function
void unzip( bigArray, littleArray1, littleArray2, sizeOfLittleArrays ) {
  for( n times ) {
    larray1[n] = bigArray[someIndex]
    larray2[n] = bigArray[someIndex+1]
  }
}
```

Compile with:
```
g++ main.c -o main.o
./main.o
```

## PROBLEM 3.15 - LOWERCASING LETTERS
Write a function that lowercases all uppercase letters in a string (char array)

> Characters (chars) are actually just numbers converted according to the ASCII table. You can reference this table to see what the underlying numeric values of letters are, and use this information to perform appropriate comparisons and transforms.

Operations you might need:
```cpp
for( int i = 0 ; i < x ; i++ ) { // Iterate x times with i moving from 0 to x-1
  
}

char c = 'A';
c += 1;    // Move 'A' to 'B'
c++;       // Move 'B' to 'C'
c = c + 1; // Move 'C' to 'D'

int n = 123;
( n > 100 ); // True if n is greater than 100
( n < 200 ); // True if n is less than 200
( n > 100 ) || ( n < 200 ); // True if n is greater than 100 OR  less than 200
( n > 100 ) && ( n < 200 ); // True if n is greater than 100 AND less than 200

char string[ 10 ];  // Char string of size 10 (9 characters + null terminator)
string[ 9 ] = '\n'; // Null-terminate a string
```

### TASK 2
> In plain English, explain how your lowercasing program should operate or - if you've already done it - how it operates. <br>
> [Back to checklist](#checklist)

```cpp
In pseudocode
```

## PROBLEM 3.16
Write a program that accepts a number from command line, then calculates the interval (term?)
e.g. "5" would yield the product of 5 x 4 x 3 x 2 x 1 (120)

Operations you might need:

```cpp
int main( int argc, char* argv[] ) {
  // Do things here
  char* theArg = argv[1];
  int number = atoi( theArg ); // Catch a number from the command line
}
```

### TASK 3
> In plain English, explain what argc and argv are. <br>
> [Back to checklist](#checklist)

```cpp
In pseudocode
```

Compile with:

```
g++ main.c -o main.o
./main.o 5
```


### TASK 4
Write a program that accepts a numeric value from command line argument (CLA). The program should instantiate an array based on the CLA, then populate that array with random numbers.

Compile with:

```
g++ main.c -o main.o
./main.o 100
```

> Call the program with the argument '100'. Provide a screenshot of your output, then compare your outputs between runs and with a peer. Notice anything odd? <br>
> [Back to checklist](#checklist)

## BUBBLESORT
Bubblesort is one of the worst (that is, slowest) sorting algorithms we have. It is, however, useful in cases where the datasets are small and we're too lazy to implement anything more complicated.

In bubblesort, we examine the first pair of elements, then place them into the correct order.

Next, we move over one and perform the same operation.

By the time we reach the end of the series, the largest element will have "bubbled" its way up to the top. This also means that the topmost element is "sorted", while all other elements are unsorted.

We then repeat this process again for all but the largest element (which is now sorted).

TODO - Describe Sorted and Unsorted subsets

The time complexity of bubblesort in the worst case may be described as O(n<sup>2</sup>).

### TASK 5
> Extend the program from Task 4 by adding a 'sort()' function that places all elements of the array into sequential order (either ascending or descending). Run this program and provide a screenshot of your outputs. <br>
> [Back to checklist](#checklist)

```cpp
In pseudocode
```

## QUICKSORT
```cpp
In pseudocode
```

##### PRE-LAB 1
[Linux Tutorial](https://docs.google.com/document/d/175SntwctNk-OKcpayJ_faGmg2SZrHFkra0yUMXkNU9w/edit?usp=sharing)
> Using the shell loop, run the program multiple times, passing different command line arguments.
> How might you use `$ grep` to isolate output lines of interest? <br>
> [Back to checklist](#checklist)

##### PRE-LAB 2
> How does isolation of the `sort()` function facilitate the testing of different sort strategies? What is the purpose of the `operations` variable? Why does it always seem to report the same number of operations despite the arrays being populated with a called to `rand()`? <br>
> [Back to checklist](#checklist)
