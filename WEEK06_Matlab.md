# MATLAB

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1 - The Perfect Binary Search Tree](#task-1)
- [ ] [Task 2 - On the Suitability of Trees](#task-2)
- [ ] [Task 3 - Matlab Plotting](#task-3)
- [ ] [Pre-lab 1](#pre-lab-1)

## ADMINISTRIVIA

### FOLLOWUPS

#### DEGENERATE TREES
![Degenerate Tree](Img/degenerate_trees.jpg)

They're just linked lists, with the same pitfalls and performance qualities. Most all binary trees have the risk of becoming linked lists and, because of that pesky **worst case** quality of time complexity, all binary trees will behave no better than linked lists _unless special controls are specified_. These controls must guarantee that the tree cannot become linked-list in shape.

This isn't always a practical concern, however. In many real-world situations, the inputs will not be delivered in the worst case order.

#### BINARY TREE CAPACITY
![Tree Capacity](Img/tree_capacity.jpg)

Starts at 1, then (doubles)+1 with every layer. Is always odd.

33 layers is enough to store every unique number represntable by a 32-bit integer.

#### BINARY SEARCH TREES (BSTs)
A tree in which the left child is always higher than the parent, and the right child is always less than the parent.

![Binary Search Tree](Img/bst.jpg)

But with a couple extra rules for insertion and finding:

##### FIND( ) ON A BST
![BST Find](Img/bst_find.jpg)

```cpp
Set root to Curr
While Curr is not nullptr
  Check to see if Curr is the value to find
    If so, return a reference to Curr
    Else if the value we're looking for is greater than Curr
      Move to the left  
    Else if the value we're looking for is less than Curr
      Move to the right

Element was not found
```

##### INSERT( ) ON A BST
Almost exactly like find, except we have to look ahead slightly so we can insert the element -before- we fall off the tree.

![BST Insert](Img/bst_insert.jpg)

```cpp
Set root to Curr
If Curr == InsertValue
  return (cause there's no point in going further, the element is already in the tree)
Else if InsertValue is greater than Curr
  Check if left is nullptr
    If so, insert element to the left
  If not, move left
Else if InsertValue is less than Curr
  Check if right is nullptr
    If so, insert element to the right
  If not, move right
```

##### DELETE( ) ON A BST
Deletion starts with a `Find()` operation to locate whether the element is in the tree at all. If it is, we flag the node for deletion.

![BST Delete](Img/bst_delete1.jpg)

However, direct removal would leave an empty node and orphan the subtrees. This may be avoided by moving the **in-order successor** or **in-order predecessor** into the location vacated by the deletion. When the IOS or IOP is a leaf, this is quite easy.

![BST Delete](Img/bst_delete2.jpg)

However, what if the IOS or IOP is itself a parent to other nodes? 

![BST Delete](Img/bst_delete3.jpg)

The result can become kind of complicated. Moving the IOS or IOP actually results in an action very similar to another delete, and now the IOS or IOP of the node we had to move must be located and so on and so forth.

This task can be entirely avoided if we use **lazy deletion**. In this schema, we simply flag the element as having been deleted (using something like an `isDeleted` field), but leave the node in place. If an element is re-added at a future time, we can just flip the deletion flag back.

##### PERFORMANCE OF BSTS
If the tree is 33 layers and **complete** (that is, all nodes have 2 children except those on the bottom and the bottom is flat), you can `find()` 1 value in 4,294,967,295 (2^32) in at most 32 comparisons.

This is the quintessential O(logN) operation, where the `find()` function's operation time grows logarithmically (that is to say, sloooowly) compared to the body of data.

> Databases took this further and use structures called B-Trees that can have an arbitrary number of child nodes, resulting in a "shallow", but "wide" tree structure. This was advantageous since physical drives were severely limited by the speed of their read operations. Moving the read-head was a very slow action, and often the bands or sectors that a read-head was positioned at was large enough to hold significantly more than just 2 reference locations. If that band could hold, say, a hundred references, then it would be ideal for the sort tree to have 100 children since the comparison to find the correct reference (even among a hundred) could be completed long before the drive is ready to read a different sector.

> For reference, the arm movement of an HDD and the spin of the sector under the arm takes about 10 milliseconds. You can perform approximately 50 million CPU operations in that time.

##### TASK 1
> THE PERFECT BINARY SEARCH TREE<br>
> Thus far, we have established that the left and right children must be greater or less than their parents, respectively, in order for a binary tree to exist. However, as we've seen, these trees can devolve in to linked lists. What other rule(s) must be enforced in order for us to guarantee that the BST will perform ideally? In other words, what else must be true such that the tree and all of its subtrees may be searched in the fewest comparisons possible? <br>
> <br>
> [Back to checklist](#task-checklist)

#### N-NARY TREES
Similar to BSTs and B-Trees, but the number of children is arbitrary. [Matrilineal descent](https://en.wikipedia.org/wiki/Matrilineality) trees are a form of N-nary tree, as are their corresponding [patrilineal descent](https://en.wikipedia.org/wiki/Patrilineality) trees.

> Fun fact! All extant humans share a common matriarchal descendant known as [mt-MRCA](https://en.wikipedia.org/wiki/Mitochondrial_Eve)! As one might expect, there's a male counterpart; the [Y-MRCA](https://en.wikipedia.org/wiki/Y-chromosomal_Adam). The mind blowing bits are that these specific individuals will shift over time as present-day bloodlines change or end, and that mt-MRCA and Y-MRCA hailed from completely different time periods.

##### TASK 2
> ON THE SUITABILITY OF TREES<br>
> Given what you know at present, briefly describe why trees might be uniquely well-suited to the challenge of cataloguing the known lineages of all humans. What about the trees lends themselves to this problem? What about the problem of lineage does **not** seem to parallel trees? <br>
> <br>
> Provide a back-of-the-napkin estimate of how many pointers (or nodes) you will require for this task. You may assume that 3 children are born every generation, that all present record extend back at most 10 generations, and that couples bond for life. <br>
> <br>
> What data type would you use for the Globally Unique ID (GUID) for every person and why? <br>
> <br>
> [Back to checklist](#task-checklist)

## PROBLEMS

### CAPTION PUNCTUATION
> If the caption didn't end with a punctuation mark (. ! ?), a period should automatically be added. A common error is to end with a comma, which should be replaced by a period. Another common error is to end with two periods, which should be changed to one period (however, ending with three periods (or more) indicates elipses so should be kept as is). The corrected caption is output.

```cpp
// Assumes that input is captured
std::string correctPunctuation( std::string input ) {
  // Find how much punctuation we're dealing with
  int numPunctuations = punctuationCount( input );
  if( numPunctuations > 3 ) {
    // This is a caption.!?...!.?
    // Probably strip all punctuation except the last mark
  }
  else if( numPunctuation == 3 ) {
    // This is a caption.!?
    // This is a caption... < The only correct caption
    // Check if it's an ellipses, otherwise keep just 1
    // Actually very difficult to tell if something should be a statement or a question
    // Natural Language Processing (NLP)
  }
  else if( numPunctuation == 2 ) {
    // Definitely incorrect
    // This is a caption?! < Arguably might be correct
    // If you can only keep one, precedent goes to the ?
    // Otherwise reduce to '.'
  }
  else if( numPunctuation == 1 ) {
    // This is fine
  }
  else if( numPunctuation == 0 ) {
    // Add a period
  }

}

// std::string n = "Hello, World!";
// n.pop_back();
// std::cout << n; // "Hello, World" expected
```

### RUNWAY HEADINGS
> Airport runways are numbered using a 2-digit number, like 09. The meaning generally is that planes taking off or landing on that runway will be facing 090 or 90 degrees rotated right from north, namely facing east. Given a runway number (integer), output the degrees followed by the closest direction indication (north, northeast, east, southeast, south, southwest, west, or northwest). If the input is 03, the output is: 30 degrees (northeast).

![Runway Headings](Img/cardinaldirections.jpg)

```cpp
#include <string>
#include <iostream>

void runwayReport( int rwNumber ) {
  int degrees = rwNumber * 10;
  std::cout << degrees << " degrees (";
  std::string directions[8] = { "North", "Northeast", "East", "Southeast", "South", "Southwest", "West", "Northwest" };
  int index = ((int)(degrees + 22.5) % 360) / 45;
  std::cout << directions[index] << ")" << std::endl;
}

int main( ) {
  for( int i = 0 ; i < 37 ; i++ ) {
    runwayReport( i );
  }
  return 0;
}
```

### WORD COUNT
> Many text processing tools like Microsoft Word or Google Docs will count the number of "words" in text. A word is a sequence of any characters other than a space. Write a program that reads an input line of text, and outputs the number of words in that text. If the input is "I have a bike." then the output is 4.

```cpp
#include <string>
#include <sstream>
#include <iostream>

int wordCount( std::string input ) {
  std::stringstream ss;
  ss << input;
  std::string omnomnom;
  int counter;
  while( !ss.eof() ) {
    ss >> omnomnom;
    counter++;
  }
  return counter;
}

int main() {
  std::string test = "This is a test. Of the emergency wordcount system.";
  std::cout << wordCount( test ) << std::endl;
  return 0;
}
```

## MATLAB
### OVERVIEW
>A proprietary multi-paradigm programming language and numeric computing environment developed by MathWorks. MATLAB allows matrix manipulations, plotting of functions and data, implementation of algorithms, creation of user interfaces, and interfacing with programs written in other languages.<br>
> From [Wikipedia](https://en.wikipedia.org/wiki/MATLAB)

Basically the industry standard for mathematics programming

> Acquire from UW technology program for free!

#### SCRIPTING
Matlab is scripted, not compiled. By default, the person executing your code must have a copy of the Matlab application (or another application that can interpret .m code) in order to run your logic.

#### FILE EXTENSION
*.m for Matlab files (also for Matlab class files)

### HISTORY
Started in the late 1970s, actually written in C and C++ and uses many of the C/C++ computational libraries for its own calculations. Includes some Fortran interfaces and a C/C++ API that can be called directly from a C++ program.

## GETTING STARTED
> Use Ctrl + C to cancel out of the command window. Can be used to halt program execution.

### CREATING A NEW FILE
For executable:
In left sidebar ("Current Folder") -> RMB -> New -> Script

For class files:
In left sidebar ("Current Folder") -> RMB -> New -> Class

### A SIMPLE PROGRAM
```matlab
format compact
% Dynamic (not strong) typing; everything is a double by default
% Use input() to retrieve user input from console
% Single or double quotes may be used for strings
% Use 's' as second argument to set return type to string
varName = input( 'What''s your name?', 's' );   % Semicolon suppresses display of variable assignment

% Tilde (~) for 'not'
if ~isempty( varName )
  fprintf( "Hello %s \n", varName );

% Line continuations
myVar = 1 + 2 + 3 ...
  + 4 + 5 + 6 + 7 ...
  + 8 + 9 + 10

%{
Multiline commenting
%}

end
```

### VECTOR INPUTS
```matlab
vinput = input( "Vector input: " ); % Expected in format: [ 2 3 4 ]
disp( vInput );
```

### VARIABLES
Storage location for data (a bucket). Does have a class type. Can be named anything with a letter, followed by letters and/or alphanumerics.

> If no variable is specified for assignment, results of an operation will be stored to the variable `ans`

#### VARIABLE CLASSES
Available class types:
```matlab
int8    - 8 bit integer
int16   - 16 bit integer
int32   - 32 bit integer
int64   - 64 bit integer
char    - ASCII characters
logical - Boolean equivalent
double  - 63-bit floating point
single  - 32-bit floating point
u       - Unsigned (uint8, uint16, etc.)
```
##### INT
Whole numbers; behave same as `ints` in C and C++. Truncates any value past the decimal point

##### CHAR
From the ASCII table; same numeric correspondence as in C and C++.

##### STRINGS
```matlab
myString = "Hello, World!"
length( myString )
myString( 1 )                                  % Character selection: 'H' expected
myString( 1:5 )                                % Substring: "Hello" expected
myString = strcat( myString, " And goodbye!" ) % Concatenation: "Hello, World! And goodbye" expected
strrep( myString, "goodbye", "GOODBYE!" )      % Find-replace: "Hello, World! And GOODBYE!" expected
```

Can also sort characters in a string, reverse sort, remove select characters, and a bunch more that isn't supported in C++ by default.
[RTFM](https://www.mathworks.com/help/matlab/characters-and-strings.html)

##### FLOATING POINTS
Singles/Doubles, bits of the number is divided into (sign bit)(exponent)(mantissa)

##### LOGICALS
Just true or false
```matlab
% 1 == true
% 0 == false
5 > 2         % ans = logical 1 expected
myBool = true % myBool = logical 1 expected
```

#### VARIABLE TYPE QUERY
Class of variable can be queried with `class( )` function:
```matlab
myChar = 'A'
class( myChar )

mySingleQuoteString = 'This is a string'
class( mySingleQuoteString ) % 'char' expected! Beware of this!

myDoubleQuoteString = "This is also a string"
class( myDoubleQuoteString ) % 'A string' expected
```

#### VARIABLE MAXIMUM AND MINIMUM
```matlab
intmin( 'int8' )
intmax( 'int8' )
realmax             % Maximum for a double
realmax( 'single' ) % Maximum for a single
```

#### VARIABLE CASTS
```matlab
myVar = 123
class( myVar )
myCastVar = int8( myVar )
class( myCastVar )

% Can also use casts with characters
charAsNumber = double( 'A' )
charByNumber = char( 65 )
```

## FORMATTED PRINTING
```matlab
myDouble = 5 + 6
sprintf( "The sum is %d", myDouble )
%f   - For float
%.#f - For decimal point control where # is the number of decimal points desired (4 by default)
%e   - For exponential notation
%c   - For characters
%s   - For string
```

## ALGEBRAIC OPERATIONS
```matlab
1 + 2
3 - 4
5 * 6
7 / 8
mod( 9, 10 )  % Modulo
exp( 11 )     % Exponent
log( 12 )     % Logarithm
sqrt( 13 )    % Square Root
deg2rad( 14 ) % Degree-to-Radian
```
And a whole ton more beside. Use `>> help elfun` in the console for a report of available math functions

> Note: Matlab does not have increments and decrements.

## OPERATORS
### RELATIONAL
```matlab
1 > 2
3 < 4
% Also <=, >=, ==, ~=
```

### LOGICAL
```matlab
% ||    - OR with short circuit evaluation
% |     - OR without short-circuit evaluation
% &&    - AND with short circuit evaluation
% &     - AND without short-circuit evaluation
$ ~     - NOT, negation
% xor   - XOR, exclusive or
% all   - Are all non-zero or true
% any   - Are any elements non-zero
% false - False no matter what

% But because logicals are a class of object in Matlab, they have some functions/capacities
% find - Find indices of non-zero elements
% islogical - Is the input a logical array?
% logical - Convert numeric values to logicals
% true - True no matter what
```

## FUNCTIONS
```matlab
function return = adderFunc( arg1, arg2 )
  return = arg1 + arg2
end

adderFunc( 2, 3 ) % 5 expected
```

### LAMBDA (ANONYMOUS) FUNCTIONS
Functions that are not named; they get used once at the location they are defined, then they are lost.

### FUNCTIONS AS ARGUMENTS AND RETURNS
Possible to pass a function as an argument and also return a function

## BRANCHING
### IF - ELSEIF - ELSE
```matlab
if ( 1 > 2 )
  disp( "1 is greater than 2!" )
elseif (1 < 2 )
  disp( "This is really strange." )
else
  disp( "How did we even get here?" )
```

### SWITCHES
```matlab
selection = 1;
switch selection
  case 1
    disp( "Do case 1 things here!" )
  case { 2, 3, 4 }
    disp( "This is a switch array" )
  case num2cell( 5:9 )
    disp( "This is a switch range" )
  otherse
    disp( "This is a catchall" )
```

## LOOPS
### FOR
```matlab
for i = 0:10 % From 0 to 10, implicit increment by 1
  disp( i )
end

for i = 10:-1:0 % From 10 to 0, decrementing by 1
  disp( i )
end

for i = [2 4 6 8] % Where i is specified
  disp( i )
end

myMtx = [ 1 2 3 ; 4 5 6 ; 7 8 9 ]
for row = 1:3
  for col = 1:3
    disp( myMtx( row, col ) )
  end
end

myVec = [ 1 2 3 4 5 6 7 8 9 10 ]
for i = 1:length( myVec )
  disp( myVec( i ) )
end
```

### WHILE
```matlab
myFlag = true
while myFlag
  disp( "Loopage!" )
  break
end
```

## VECTORS
```matlab
myVector = [ 1, 3, 4, 2, 5 ]            % Vector initialization
vecLen = length( myVector )             % Length of vector
myVector = sort( myVector )             % Sort ascending order
myVector = sort( myVector, 'descend' )  % Sort descending order
myVector( 1 )                           % Access the first element of myVector - NOT 0-BASED INDEXING!
myVector( 20 ) = 123                    % Places 123 at 20th position, 0's in between
myVector( 5:10 )                        % Returns the range of elements from 5 to 10
myVector( [2 4 6 8])                    % Return elements 2, 4, 6, and 8
colVector = [ 1 ; 2 ; 3 ]               % Generates a column vector
```

### VECTOR RANGES
```matlab
myVector = 1:10            % 1 to 10, increments of 1
myVector = 1:2:10          % 1 to 10, increments of 2
concatVector = [vec1 vec2] % Concatenation of vec1 and vec2
myVector = linspace( 1, 20, 4 ) % 1 - 20, spaced by 4
myVector = logspace( 1, 3, 3 )  % Logarithmically spaced vectors
```

### VECTOR MULTIPLICATION
```matlab
rowVec = [ 1 2 3 ]
colVec = [ 4 ; 5 ; 6 ]
result = rowVec * colVec
```

### DOT PRODUCT
```matlab
dotProd = rowVec * colVec'
dotProd = dot( rowVec, colVec )
```

### CROSS PRODUCT
```matlab
crossProd = cross( rowVec, colVec )
```

## MATRICES
### INSTANTIATION
```matlab
myMatrix = [ 2 3 4 ; 5 6 7 ]
mLen = length( myMatrix )         % Width
count = numel( myMatrix )         % Absolute count of matrix elements
mSize = size(  myMatrix )         % Matrix size (row, then column)
[ rows, cols ] = size( myMatrix ) % Size of matrix in rows, columns

randoMatrix = randi( [0, 100], 4 ) % 4 x 4 matrix of random numbers from 0-100
randoMatrix( 1, 2 ) % Access element at row 1, column 2; can assign with '='
randoMatrix( :, 2 ) % Access all row elements, column 2; can assign with '='
randoMatrix( 2, : ) % Access row 2, all column elements; can assign with '='
randoMatrix( end, end ) % Access the last row, last col; can assign with '='
```

### MATRIX ALGEBRA
```matlab
% Matrix multiplication
mtx1 = [ 1 2 3 ;
         4 5 6 ]
mtx2 = [ 1 1 1 1 ;
         2 2 2 2 ;
         3 3 3 3 ]
mtxProd = mtx1 * mtx2
```

### MATRIX COMPARISONS
```matlab
% Logical result of comparison
mtx2 = [ 1 1 1 1 ;
         2 2 2 2 ;
         3 3 3 3 ]
mtxIsGreaterThan = mtx > 2

% [ 0 0 0 0 ;
%   0 0 0 0 ;
%   1 1 1 1 ]
% Expected
```

### AND MORE!
[RTFM](https://www.mathworks.com/matlabcentral/fileexchange/47533-cheatsheet-pdf)

## CELLS
Like a struct, can store an arbitrary number of data objects
```matlab
myCell = { 'A string!', 123, [1 2 3] } % Storing a string, double, and vector
```

## ACTUAL STRUCTS
```matlab
aPerson = struct( "name", "John Doe", "age", 40 )
disp( aPerson.name )

% Supports field concatenation after definition
aPerson.newField = "Voila!"

% Field can also be removed
aPerson = rmfield( aPerson, "newField" )
```

[RTFM](https://www.mathworks.com/help/matlab/ref/struct.html)

## CLASSES
Support for OOP coding:
```matlab
classdef BasicClass
   properties
      Value {mustBeNumeric}
   end
   methods
      function r = roundOff(obj)
         r = round([obj.Value],2);
      end
      function r = multiplyBy(obj,n)
         r = [obj.Value] * n;
      end
   end
end
```

[RTFM](https://www.mathworks.com/help/matlab/matlab_oop/create-a-simple-class.html)

## TABLES
Array in tabular form with named columns (just like a spreadsheet)
Type `help table` in the console to bring up the Help file for tables
[RTFM](https://www.mathworks.com/help/matlab/tables.html)

## FILE I/O
Matlab also supports file input/output

```matlab
% Assuming you have a matrix...
save myDataFile.dat myMatrix -ascii
load myDataFile.dat
disp myDataFile
type myDataFile.dat
```

### TASK 3
> PLOTTING IN MATLAB<br>
> Attempt to run the following code<br>

```
load patients Height Weight Systolic    % load data
scatter(Height,Weight)                  % scatter plot of Weight vs. Height
xlabel('Height')
ylabel('Weight')
```
> Provide a brief summary of what 'load patients Height Weight Systolic' does. What else are you able to 'load'? <br>
> <br>
> [Back to checklist](#task-checklist)

## PRELAB 1
> Using the following binary search tree:

![Binary Search Tree](Img/bst.jpg)

> Imagine that each node represents a room in a floor plan. In every room, there is a checklist of tasks glued to the floor. Each checklist is identical, and they all contain the following:

```
Attempt to go through the left door
Read the number in this room
Attempt to go through the right door
Return to the room you came from
```

> If you use the checklist in whatever room you're in, what numeric sequence will you end up reading out? <br>
> <br>
> [Back to checklist](#task-checklist)
