# WEEK 2 - C LANGUAGE SYNTAX

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1](#task-1)
- [ ] [Task 2](#task-2)
- [ ] [Task 3](#task-3)
- [ ] [Task 4](#task-4)
- [ ] [Task 5](#task-5)
- [ ] [Pre-lab 1](#pre-lab-1)
- [ ] [Pre-lab 2](#pre-lab-2)

## ADMINISTRIVIA

### FOLLOW-UPS
#### AUTO-EXECUTION
Assuming that a USB device, when connected, may communicate in some fashion with the computer that it is connected to, are we **CERTAIN** that no arbitrary string of inputs will yield root-level and/or remote access to the device? There is at least one string, which are the commands a legitimate administrator would use for this purpose, but that string usually involves a secret (a password).

Fundamentally, an exploit is just a string input that doesn't require the system secret to gain control.

But are passwords actually secure?

https://nordpass.com/most-common-passwords-list/

https://en.wikipedia.org/wiki/Keystroke_logging

Weaponized USB devices designed to subvert computer security are widely available for only a few dollars.

https://shop.hak5.org/collections/hotplug-attack-tools

Leads to 'dead-drop' attacks.

https://en.wikipedia.org/wiki/2008_cyberattack_on_United_States

#### POWER CONSUMPTION AS PERFORMANCE
There are a lot of mobile and low-energy situations in computing where a faster solution might require more power. Engineering is the practice of identifying your priorities and constraints, then discovering the optimized solution for that context.

## WORKING IN LINUX
### COMPILING
```
gcc <PROGRAM_NAME>.c -o <OUTPUT_NAME>
```

### RUNNING
```
./<PROGRAM_NAME>
```
The `exit()` command may be placed in code to specify an arbitrary termination point that is not the closing brace of main().

##### TASK 1
> Write a shell script that can encapsulate and automate your compile and run commands. <br>
> You may optionally use it by modifying its permissions with `$ chmod u+x <SCRIPTNAME>`, then execute it with `./<SCRIPTNAME>` <br>
> [Back to checklist](#checklist)

## COMMENTS
Text for human authors/readers that are ignored by the compiler
```cpp
/*
Multiline comment
*/

// Line comment
```

## INCLUDES
Inserts the contents of the designated library at the location of the include statement.
```cpp
#include <stdio.h> // For standard input/outputs
#include <string.h> // For String objects
```

## CONSTANTS
At compile time, replaces the variable name with the string to insert, then continues to compile as normal.
```cpp
#define STRING_TO_REPLACE "String to insert"
```

## VARIABLES
```cpp
int myVariable; // Variable declaration
myVariable = 1; // Variable assignment
int anotherVariable = 2; // Variable declaration and assignment
int var1, var2; // Compound variable declaration
```

### SCOPE
Variables are accessible in all sub-scopes to where they are declared. They are **not** accessible to super-scopes.

Variables are returned to the general pool of memory (that is, they are deallocated) when their scope closes.

### ARRAYS
```cpp
int numberArray[4] = {0, 1, 2, 3};
```
Arrays in C are 0-Indexed. There are **no safety guarantees** in C with arrays. You can overwrite the boundaries.

### GLOBAL VARIABLES
Variables that are accessible to all functions within a program.
```cpp
int myGlobalVariable = 12345; // Set outside the scope of main
```

## MAIN (DRIVER)
```cpp
main() {
  // Do things here
}
```

##### TASK 2
> Write a 'Hello, World!' program in C. Compile and run it (perhaps using the script from Task 1) <br>
> [Back to checklist](#checklist)

## PRIMITIVE DATA TYPES
| NAME | BITS | DESCRIPTION |
| :--- | :--- | :--- |
| `char` | 8 | ASCII text character |
| `int` | 16 | Integer value |
| `float` | 32 | Floating point number |
| `bool` | 8 | Boolean true/false |
| `void` | 8 | Nothing |

Sizes are defined as minimums, you might get more depending upon how your compiler handles things, but you won't get less.

Use of `short` and `long` prefixes can modify the number of bits you will receive. Consult the documentation if this is ever an issue.

Use of `signed` and `unsigned` prefixes can modify chars and ints into their signed or unsigned formats.

### ESCAPE CHARACTERS
Consult documentation for full listing if other escape characters are needed

```
\n - Newline
\t - Tab
\\ - Backslash
\" - Quotation mark
```

### SIZEOF()
Allows program to query the byte-size of a data type.
```cpp
sizeof( char ); // Returns the number of bytes that a char uses in this program environment
```

### BINARY REPRESENTATION
All data is stored in binary, almost always in even byte divisions (8 bits).

8-bit Chars may have a state between 0-255 (256 discrete states), corresponding to a letter off the ASCII table.

Signed Ints are [Two's Complement](https://en.wikipedia.org/wiki/Two%27s_complement)

### OVERFLOW
Counting beyond the limits of a datatype results in an overflow condition. No native protection is provided against this condition.

## PRINTING
Simple print:
```cpp
int age = 1234;
printf( "I am %d years old \n\n", age );
```

Use conversion characters for differing datatypes:
| | |
| :--- | :--- |
| %d | int |
| %ld | long int |
| %.5f | float to 5 decimal places |
| %.5f | double to 5 decimal places |
| %c | char |
| %s | string |
| %lu | long unsigned int |

## C STYLE STRINGS
No string object class provided. Create a character array instead. Create an array of _at least_ the size of the string plus 1. Strings must be "null terminated" (that is, a special character `/0` must be placed after the end of the last character of the string)


##### TASK 3
> In your own words, describe what 'null termination' is and why it exists. <br>
> [Back to checklist](#checklist)

```cpp
char hello[14] = "Hello, World!"; // Auto null terminated
char hello[] = "Hello, World!"; // Auto-sized and null terminated
char goodbye[8] = { 'G', 'o', 'o', 'd', 'b', 'y', 'e', '\0', }; // Must be null-terminated manually this way
```

Cannot naively re-assign values to the string (char array). Must use the `strcopy()` method from <string.h>

```cpp
strcpy( hello, "Goodbye, World!" );
```
### STRING COMPARISON
```cpp
#include <stdlib.h>
strcmp( string1, string2 );
```
If `string1 > string 2`, strcmp() returns a positive value
Else if `string1 < string2`, strcmp() returns a negative value
Else if `string1 == string2`, strcmp() returns 0

##### TASK 4
> In Unix/Linux systems, it is advised that you name your folders/directories starting with a capital letter. Based upon what you know of the ASCII table, why might this advice be given? <br>
> [Back to checklist](#checklist)

### STRING CONCATENATION
```cpp
strcat( string1, string2 );
```
`string1` now contains the concatenation of both `string1` (original) and `string2`.

### STRING LENGTH
```cpp
strlen( string1 );
```
Returns the length of `string1`.

### STRING COPY
> Avoid the use of `strcpy()` due to memory overwrite risk
```cpp
strlcpy( string1, string2 );
```
`string1` now holds the value of `string2` (but safely)

## USER INPUT CAPTURE
### WITH PRINTF
Capture of char input:
```cpp
char inputChar;
printf( "This is a prompt! " );
scanf( " %c", &inputChar );
```

You may also capture Strings
```cpp
scanf( " %s" ); // Capture of a string
scanf( " %s %s " ); // Capture of two strings, space delimited
```

### WITH FGETS
```cpp
char inputString[25];
fgets( inputString, 25, stdin );
```

## ARITHMETIC

| OPERATOR | DESCRIPTION |
| :--- | :--- |
| `+` | Addition |
| `-` | Subtraction |
| `*` | Multiplication |
| `/` | Division |
| `%` | Modulo |
| `++`, `+=` | Increment |
| `--`, `-=` | Decrement |

C supports pre- and post- increments and decrements. Placing the operator before or after a variable controls whether the operation is performed before or after (TODO).

### ORDER OF OPERATIONS
1. Parentheses
2. Negative sign
3. (!) Negation
4. Increment/Decrement
5. Multiplication/Division
6. Modulus
7. Addition/Subtraction
8. Relational Operators

When in doubt, use parentheses to disambiguate

## FLOW CONTROL

### COMPARISON OPERATORS
```cpp
( 1 > 2 ); // Resolves to (int)'0' - false
( 1 < 2 ); // Resolves to (int)'1' - true
// Also have ==, <=, >=
```

### CONDITIONAL OPERATORS
Similar to comparison operators, but returns a programmer defined value on true or false (both returns must be defined)

### IF - ELSE IF - ELSE
Only one clause may execute, and it will be the clause controlled by the first clause that evaluates to true.
```cpp
if( <CONDITION> ) {
  // Do a thing
}
else if( <CONDITION> ) {
  // Do another thing
  // Note short-circuit evaluation
}
else {
  // Do for everything else
}
```

#### TERNARY OPERATORS
Pretty uncommon and you don't need it for this course, but If/else clauses may be truncated using a ternary operator (`?`). This is mostly included just so you recognize it if you come across one.

```cpp
char gender = ( response == "Female" ) ? 'F' : 'M';
```

#### SWITCH
A switch statement may specify multiple execution branches in a different format:
```cpp
int choice = 0;
switch( choice ) {
  case( 0 ) : // Do the thing for Case 0
              break;
  case( 1 ) : // Do the thing for Case 1
              break;
  case( 2 ) : // Do the thing for Case 2
              break;
  default   : // Do the thing for all other cases
              break; // Omitting the break here will cause the switch to loop back up
}
```

### LOGICAL OPERATORS - AND, OR, NOT
Boolean true and false may be related to other true or false values using logical operators.

`&&` - "And", True where (T && T), False otherwise
`||` - "Or", True where (T || F, F || T, T || T), False otherwise
`!` - Not, True where (!F), False otherwise

There are actually 16 logical operators, but these three are your bread and butter.

#### XOR
'XOR' (pronounced 'Ex-Or', short for 'Exclusive Or') is another popular logical operator in computer science (though not as popular as the others). Similar to others, it resolves to 'True' or 'False' based upon the states of its two inputs. XOR resolves according to the following rules:

| Input 1 | Input 2 | Result |
| :--- | :--- | :--- |
| T | T | F |
| T | F | T |
| F | T | T |
| F | F | F |

> Basically, it's True if both inputs are different, False otherwise.

It is used extensively in cryptographic applications due to the unique trait that if a sequence of bits is used as a key, then both the encryption and decryption may be performed using the same operation (that is, the XOR). Here, the 'ciphertext' is the result of an encryption action on a sequence, while the 'plaintext' is the original message and the decrypted result (hopefully these two are the same!)

By way of example:
|  |  |
| :--- | :--- |
| Message |    0 1 0 1 0 0 1 1 |
| Key |        0 0 0 0 1 1 1 1 |
| Ciphertext | 0 1 0 1 1 1 0 0 |

Next, we take the ciphertext and "encrypt" it with the same key we used previously:

|  |  |
| :--- | :--- |
| Ciphertext | 0 1 0 1 1 1 0 0 |
| Key |        0 0 0 0 1 1 1 1 |
| Decrypt |    0 1 0 1 0 0 1 1 |

Finally, a comparison of the original message and the decrypted state reveals that we have performed a successful "round trip". The message has been encrypted and decrypted successfully.

|  |  |
| :--- | :--- |
| Message |    0 1 0 1 0 0 1 1 |
| Decrypt |    0 1 0 1 0 0 1 1 |

In C, the XOR operation may be performed on single boolean values the same as `||` and `&&`. It may be applied in a bit-wise fashion like the one shown above to create a new bit sequence based upon two inputs. To use it as a logical operator that resolves to a boolean, you may note that `!=` accomplishes the same effect **for booleans**:

```cpp
if( condition1 != condition2 ) {
  // Because   1 != 0 is True
  // Likewise, 0 != 1 is True
  //           1 != 1 is False
  //           0 != 0 is False
  // True if both inputs are different, False otherwise, that's an XOR
}
```

As such, no XOR logical operator is provided in the C language.

However! You're probably going to have to use it as a 'bitwise XOR', which performs the operation shown in the example above. To do this, you may write the following:

```cpp
char plaintext  = 'A';
char key        = 'B';
char ciphertext = plaintext ^ key; 
```

> Note that based upon the nature of the XOR operator, the order of your inputs does not affect the outcome.

## LOOPS
### FOR LOOPS
Execute a block of code for a prescribed number of iterations (`i`) (actually, while a condition is true)
```cpp
for( int i = 0 ; i < 10 ; i++ ) {
  // Do the thing
}
```

### WHILE LOOPS
Executes a block of code while a condition evaluates to true.
```cpp
while( 1 ) { // '1' may be used in lieu of 'True' and '0' for 'False'
  // Keep doing the stuff in here
}
```

#### DO WHILE LOOPS
Just a while loop, but it will execute **at least** once before the while condition is evaluated.

#### BREAK AND CONTINUE
The `continue` keyword may be used to advance to the next loop iteration. <br>
The `break` keyword may be used to escape a loop.

## FUNCTIONS AND SUBROUTINES

### DECLARATION
Function declaration must precede reference. Function definition (implementation) may occur anywhere at or after the declaration.

```cpp
int myFunction( char arg1 );
```

### IMPLEMENTATION
```cpp
// Integer return type, single char argument
int myFunction( char arg1 ) {
  // Do function things
  return 0;
}
```
## POINTERS / REFERENCES
```cpp
int myNumber = 1;
int* myNumberReference = &myNumber;
// myNumberReference <- The
```

> Can print pointer's value (an address of some other variable) using `%p` in `printf()`.
- [ ] Structs

### POINTER DEREFERENCE
> How to get the thing a pointer points at
```cpp
int myNumber = 12345;
int* myNumberReference = &myNumber;
*myNumberReference; // Resolves to 12345
```
> Also resolves to the zeroeth index of an array. Array indices may be targeted using pointer arithmetic (e.g. `*(myArray+2)` yields the 2nd index of `myArray`).

## CLASSES

This is C, not C++.

## STRUCTS
Similar to an array, but a struct definition acts as a template for dissimilar data elements
```cpp
struct myStruct {
  char charVar;
  int intVar;
  myStruct* nextStruct;
  char* myStringVar;
};

void main() {
  struct myStruct test1 = {
    'c',
    12345,
    NULL,
    "Foo Bar Baz"
  };
}
```
| charVar | intVar | *nextStruct | *myStringVar |
| :--- | :--- | :--- | :--- |

Copy-assignment of one struct to another copies the binary state of the struct literally. Pointers in the struct will point to the same memory addresses.
```cpp
struct myStruct test1 = { ... }
struct myStruct test2 = test1; // test2's internal state is the same as test1
```

## A LINKED LIST
```plantuml
@startmindmap
* HEAD
** MID
*** TAIL
@endmindmap
```

```cpp
#include <stdio.h>
#include <stdlib.h>

struct Node {
  int value;
  struct Node* next;
};

int main() {
  struct Node* head = NULL;
  head = (struct Node*)malloc( sizeof( struct Node ));
  head->value = 123;

  struct Node* mid = NULL;
  mid  = (struct Node*)malloc( sizeof( struct Node ));
  mid->value = 456;

  struct Node* tail = NULL;
  tail = (struct Node*)malloc( sizeof( struct Node ));
  tail->value = 789;

  // Connect the nodes together
  head->next = mid;
  mid->next = tail;

  // Traverse the list
  struct Node* curr = head;
  while( curr != NULL ) {
    // Do the thing
    printf( " %d", curr->value );
    // And move to the next node
    curr = curr->next;
  }

}
```

##### TASK 5
> Implement a standalone function that accepts the linked list above and removes the middle node from the sequence. <br>
> [Back to checklist](#checklist)

##### PRE-LAB 1
> To date (and for a very long time to come), your program inputs have been text-based and your program outputs were rendered to console. Consider some of the problems you might be able to address just using text and text processing and provide your thoughts on the matter. What is the loftiest real-world problem that can be solved by text operations?<br>
> [Back to checklist](#checklist)

##### PRE-LAB 2
> Do a bit of research on the most common or most useful Linux/Unix commands and processes. Select your top 5 to 10 and provide a listing and brief (sentence fragments are fine) summary of what the command or process does. <br>
> [Back to checklist](#checklist)
