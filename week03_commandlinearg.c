#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
  // argc is 2 (program name + argument you provide)
  // argv[0] is main.out (or whatever the executable name is)
  // argv[1] is "123"
  char* theArg = argv[1];
  int caughtNumber = atoi( theArg );
  printf( "%d \n", caughtNumber );
  
  int product = 1;
  for( int i = 1 ; i <= caughtNumber ; i++ ) {
    product *= i;
  }

  printf( "%d \n", product );

}
