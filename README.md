# UWB CSSSKL 133 (A)

TABLE OF CONTENTS

[[_TOC_]]

### ZOOM MEETING LINK + TIME
| | |
| :--- | :--- |
| When | Friday 11:30 - 2:00 |
| Where | https://washington.zoom.us/j/9475474016 |

#### SESSION RECORDINGS
Efforts will be made to make lab recordings accessible. Note, however, that this is not a guarantee. If a recording is missed, the course is not responsible for providing the content covered.

- Week 1 - [RECORDING LINK](https://washington.zoom.us/rec/share/-5gPB9rUDzacngZs48LvIBeydjnubXMGwOrAvSntx0a-bS7w9QhBBqjtTsjlIdSg.g147kow59_P65iCt?startTime=1610124234000)
- Week 2 - [RECORDING LINK](https://washington.zoom.us/rec/share/fDVXjxy_UQW7I8aaqNOlKvgXnNTLUJ2vu8iKPJx8l7oMTb-wzY-uM1TLzPG12fZL.eFMpKzPk6S5TocND?startTime=1610739175000)
- Week 3 - [RECORDING LINK](https://washington.zoom.us/rec/share/HFIGQ5BgMlB1XK5Gcd4asiJm6bdKnbDkJ8GvFCsVB1_q5565xtJwkypMFho95Zr_.uoptfcpsM8_MlRWK?startTime=1611343638000)
- Week 4 - [RECORDING LINK](https://washington.zoom.us/rec/share/diAR0HLX9BudYA7ISeA6engFU1ijVVkKDSRmoBS20WhTFQKRGPUzjiNe9gBVW6YJ.xHWnsmCseBbB5vt1?startTime=1611948606000)
- Week 5 - [RECORDING LINK](https://washington.zoom.us/rec/share/HosFyfvEgh_xmFowFyN61acpav0N-lscI4p3CYGzQF3RjHbohy8qNneU8eacorWF.DclY-n7-JYKlfNGi?startTime=1612554456000)
- Week 6 - [RECORDING LINK](https://washington.zoom.us/rec/share/9h5NJNGwwCbhoD3Nw5aeWOPXwXXISKRIfxd4-cD2MGFGEo1fP5DdB5zYzqwLUUZO.VGdaAVAS5nAvRz0E?startTime=1613158214000)
- Week 7 - [RECORDING LINK](https://washington.zoom.us/rec/share/v7ToOvKeMpT2QfpQiIRzaxuMzlZ539vM_o4KyA3miGHZyFeVzS7LDd-an-nYl25s.z_A-Kr5x0H9_1tUC?startTime=1613763069000)
- Week 8 - [RECORDING LINK](https://washington.zoom.us/rec/share/gLj2BGGbhsI_a9f639RtCJj3JSEQPVsCZmQOruOU5A6RUhI9lG5rxiu2jEI_ojE2.Rr4Rb_TuAXHTmS0A?startTime=1614368017000)
- Week 9 - [RECORDING LINK](https://washington.zoom.us/rec/share/WKpzNLUe4Go61cOCwjX5nUJIQ9ShbIR0DYoSEvw5SBOCynXKiICBvZX0iHgI-Iu1.9sYbs1bIwymAbymd?startTime=1614972705000)
- Week 10 - None
- Week 11 - Final Exams, no laboratory or recording.

### ASSIGNMENT LISTING
- [Week 1 - Intro](WEEK01_WelcomeToLinux.md)
- [Week 2 - C Mechanics I](WEEK02_Mechanics.md)
- [Week 3 - Unix/Linux, C Mechanics II, Bubblesort, Quicksort](WEEK03_Unix.md)
- [Week 4 - Classes, Stacks, Queues](WEEK04_ObjectOrientedProgramming.md)
- [Week 5 - Sequential Structs, Recursion, Graphs](WEEK05_SequentialStructs.md)
- [Week 6 - Matlab](https://gitlab.com/teabean/uwb_cssskl133/-/blob/master/WEEK06_Matlab.md)
- [Week 7 - Recursion](https://gitlab.com/teabean/uwb_cssskl133/-/blob/master/WEEK07_Recursion.md)
- [Week 8 - TBD](lab01.md)
- [Week 9 - TBD](lab01.md)
- [Week 10 - TBD](lab01.md)
- Week 11 - Nothing!

#### DUE DATES
Lab submissions are due at midnight (11:59 PM) the Wednesday following the lab session.

Corrections may be submitted up to one week following the original due date.

#### TURN-IN LOCATION

https://canvas.uw.edu/courses/1444060

#### SPECIAL INSTRUCTIONS
If alterations to the course plan occur, they will be noted here:

- Week 1 - None
- Week 2 - Please include 5 to 10 Unix/Linux command summaries on the shared spreadsheet
- Week 3 - None
- Week 4 - None
- Week 5 - Share your binary tree drawing to the class Discord
- Week 6 -
- Week 7 - 
- Week 8 - 
- Week 9 - 
- Week 10 - 
- Week 11 - 

## COURSE LOGISTICS
### OFFICE HOURS
By appointment (but seriously, Discord me whenever)

### CONTACT
| | |
| :--- | :--- |
| Name | Tim Lum |
| Discord Channel | https://discord.gg/5m48QuM |
| Discord Handle | teabean#2682 |
| Email | tlum@uw.edu |

### COURSE GRADING
Course is graded Credit/No Credit

89.9% required to pass

Grading is based on effort and your ability to submit work in a timely fashion (ship what you got)

4/4 == Strong effort made to solve all problem sets

1-3/4 == Partial problem set solution attempted

0/4 == No turn in or no alteration made to turn in

### INCLEMENT WEATHER POLICY

If labs are cancelled because of inclement weather, the UW Bothell main page will post the announcement at http://www.bothell.washington.edu/ . From this page you can also automatically get current information on campus closures and delays in the event of inclement weather and emergency situations by signing up for text message and email alerts through the UW Bothell Alert System. http://www.bothell.washington.edu/alert/ 

## ERRATA

### MISSION
To provide logical and technical tools to assist you in improving as a computer scientist and software developer.

### DESCRIPTION
This lab section is a companion to the CSS 133 course. 

It provides hands-on practice and experience as well as guides you in implementing programming concepts.

The goal is to get introduced to computational thinking, problem solving, practice-practice-practice writing code and debugging problems, and gain practical experience.

Subjects of this course should help you with other programming aspects of CSS 133 (e.g. homework), as well as reinforce the concepts being taught. 

The topics covered  in this lab will be synchronized with those in the CSS 133 lectures, and are potentially subject to change as a result.  

### LAB REQUIREMENTS  

No requirements are placed on the execution of the lab content; you may develop your code in whatever environment or pipeline you are most comfortable with.

Completed work should be submitted as a text file to Canvas.

## STUDENT CONDUCT

### PLAGIARISM AND CHEATING

You are expected to provide original work based on your own effort for this course. It is your responsibility to know and uphold the Student Conduct Code for the University of Washington (available at www.uwb.edu/students/policies).

Any coursework which is plagiarized or otherwise in violation of the Student Conduct Code will receive a zero and may be referred to the University for further action.

### COURTESY AND TECHNOLOGY

As this is a course on computer science, a computational device will almost certainly need to be used in class. We cannot enforce or police the appropriate usage of your devices, however, and thus politely request that you endeavor to avoid using your phone, tablet, or computer in a manner which might distract you or your peers in the classroom during lab hours.

### PARTICIPATION AND ATTENDANCE

The CSSSKL 132 course grade is derived entirely from the submission of completed labs. While it is possible to succeed in the course on paper, it is our hope that the lab sessions will provide you with a number of habits, intuitions, and skills that will serve you throughout your academic and professional career. As such, even if you are able to complete the labs without attending the session, I advise that you attend.

## ASSISTANCE

### ACCESS AND ACCOMODATIONS

Your experience in this class is important to us, and it is the policy and practice of the University of Washington to create inclusive and accessible learning environments consistent with federal and state law. If you experience barriers based on disability, please seek a meeting with Disability Resources for Students (DRS) to discuss and address them. If you have already established accommodations with DRS, please communicate your approved accommodations to your instructor at your earliest convenience so we can discuss your needs in this course.

DRS offers resources and coordinates reasonable accommodations for students with disabilities.  Reasonable accommodations are established through an interactive process between you, your instructor(s) and DRS.  If you have not yet established services through DRS, but have a temporary or permanent disability that requires accommodations (this can include but not limited to; mental health, attention-related, learning, vision, hearing, physical or health impacts), you are welcome to contact DRS at 425.352.5307 rlundborg@uwb.edu .

### RELIGIOUS ACCOMODATIONS

Washington state law requires that UW develop a policy for accommodation of student absences or significant hardship due to reasons of faith or conscience, or for organized religious activities. The UW’s policy, including more information about how to request an accommodation, is available at Faculty Syllabus Guidelines and Resources. Accommodations must be requested within the first two weeks of this course using the Religious Accommodations Request form available at https://registrar.washington.edu/students/religious-accommodations-request/ (Links to an external site.) 


