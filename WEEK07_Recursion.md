# RECURSION

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1 - Real World Recursion](#task-1)
- [ ] [Task 2 - The Grocery Line](#task-2)
- [ ] [Task 3 - The Grocery Line II](#task-3)
- [ ] [Task 4 - The Concert Hall](#task-4)
- [ ] [Task 5 - Queue Traversal](#task-5)
- [ ] [Task 6 - Tree As Array](#task-6)
- [ ] [Task 7 - Binary Tree Example](#task-7)
- [ ] [Task 8 - Tree Traversal By Sailboat](#task-7)
- [ ] [Pre-lab 1](#pre-lab-1)

## ADMINISTRIVIA

### FOLLOWUPS

#### TERMINATOR 2 AND THE TECHNOLOGICAL SINGULARITY

## ON RECURSION
Let us just first define recursion. Per [Wikipedia](https://en.wikipedia.org/wiki/Recursion_(computer_science)):

> In computer science, recursion is a method of solving a problem where the solution depends on solutions to smaller instances of the same problem.

Accurate, but also fairly abstract, let's take a look at one of the simplest examples that fits this description:

```
int sum = 1 + 2 + 3 + 4 + 5 + 6 + 7;
```

In this case, the solution can be found by successively solving the leftmost terms, then recurring (that is, doing the same thing again). The nature of the problem remains unchanged at every step.

```
int sum = 1 + 2 + 3 + 4 + 5 + 6 + 7;
              3 + 3 + 4 + 5 + 6 + 7;
                  6 + 4 + 5 + 6 + 7;
                     10 + 5 + 6 + 7;
                         15 + 6 + 7;
                             21 + 7;
                                 28;
```

If you look at this example, chances are faily high that you've been using just this recursive strategy since an early age, you probably just never called it that. Despite the bad rap that recursion receives ("Ooo... it's scary complex!"), many problems naturally lend themselves to - or cannot be solved without - the use of a recursive strategy.

### TASK 1
> REAL WORLD RECURSION<br>
> Consider a few real-world examples of problems that must be solved recursively. These may be surprisingly simple. For instance, reaching the top floor of a building involves taking a step up, then climbing the remaining stairs in the selfsame fashion. Eating a whole bag of mangoes can be achieved by eating one piece, then eating the rest of the bag using the same strategy. <br>
> <br>
> [Back to checklist](#task-checklist)

But wait, you say, isn't that just a loop? And the gig is up. This is just a very fancy way of saying `while( the problem is not solved ), keep doing manageable subsets of the problem`.

Let's now try to create a recursive solution to a problem.

### TASK 2
> THE GROCERY STORE LINE<br>
> You arrive at your local grocer, but there's a long queue to enter. You are concerned about the hours of operation and wish to know the closing time posted on the front door. Given only the ability to communicate with the person ahead of you in line, what will you ask them and/or instruct them to do in order to receive your answer? Individuals in the line may only communicate with persons adjacent (so there's no shouting to the front). You may assume that individuals will comply with your instructions to the letter. <br>
> <br>
> [Back to checklist](#task-checklist)

To formalize this, we would say that the problem of (finding the store hours from the back of the line) is in fact the same as (finding the store hours from the back of the line) - 1. We reduce the problem size by 1 on every recursive call until somebody is staring at the store hours, which they may then return (this is called the **trivial case**, when a task is complete by default or it may be solved in a single operation).

### TASK 3
> THE GROCERY STORE LINE II<br>
> Consider the following: Given the same constraints as in Task 2, could you acquire the following information:<br>
> A) How many individuals are in line ahead of you<br>
> B) Whether your friend, Karly, is in the queue ahead of you, and how far ahead she is<br>
> C) The total types and quantities of all the grocery items everybody in line needs that day <br>
> <br>
> [Back to checklist](#task-checklist)

Really, when you start to get creative with this, there are very few limits as to what you can accomplish, but it does take a bit of up-front work to plan and anticipate what action you must define at the back of the line in order to have the appropriate effect at the front. This is, really, the heart of designing recursive operations.

### TASK 4
> THE CONCERT HALL<br>
> We are not necessarily limited to operations that occur in a line. Suppose you are seated in a concert hall and, due to a sudden fit of curiosity, you simply must know how many audience members are in attendance that evening. What could you ask of your adjacent concert-goers in order to receive an answer to this question?<br>
> Be cognizant of how this situation differs from the grocery queue. Individuals will execute their instructions independent of one another, and you cannot guarantee a particular order of operations. In addition, you must be careful not to double-count members of the audience. <br>
> <br>
> [Back to checklist](#task-checklist)

In all the tasks above, you have probably noticed that for whatever instructions you send off into the queue or crowd, a special clause must be included that allows the instruction to halt at an appropriate time (that is, at the front of the line, at the edge of the crowd, if there is nobody left to ask the question to, etc., etc.). This boundary is known as the **base case**. Simply put, the base case is defined by the conditions needed to turn the recursion around.

> Note: The Base Case is often where useful work occurs, but it is not necessarily the only such place. Work may occur at any point of the recursive call stack; you can imagine if your instructions to the queue included a command to increment the first letter of one's name, the condition of the queue would be quite different on the way back than on the way there.

## RECURSIVE FUNCTION CALL OR FUNCTION DEFINITION
Recursion is very often used synonymously with "self-reference". As we've established, however, recursion is a bit broader than that. However, in computer science, recursion has this association since often a self-referential function call lends itself directly to a recursive strategy. As such, these meta-functions that call themselves are known as recursive functions, whether we like it or not.

## RECURSION ON THE TREE
Last week, we attempted to apply a particular set of operations on our binary trees. As a quick refresher, the binary search tree in question was the following:

![Binary Tree](Img/bst.jpg)

The steps you were asked to perform at every node were:
```
Step 1) Attempt to go to the left child
Step 2) Read the value of this room
Step 3) Attempt to go to the right child
Step 4) Return from where you came from
```

Similar to the grocery store queue or a concert hall, this is an example of a recursive procedure. Let's modify it slightly to make this more obvious:
```
Step 1) Attempt to Recur (do this procedure) on the Left child
Step 2) Perform work (read the value at this location)
Step 3) Attempt to Recur (do this procedure) on the Right child
--- BASE CASE --- There are no more children to recur on
Step 4) Return() to wherever this call was made from
```

If faithfully executed on the above tree, this procedure generates (somewhat as if by magic), the following sequence of numbers:

```
15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
```

Let's look a little bit more closely at this procedure:

### TREE TRAVERSAL
The first part of this process is known as a 'Traversal'. You've performed this on linked lists or really anywhere that you've iterated sequentially across a range of elements. All it means in the context of a tree is that we're going to move across every element in the tree until some condition is met. While daunting at first blush (how do you guarantee you've hit every element without repeating yourself?), a recursive function call lends itself extremely well to this purpose.

Moving across the tree, in pseudocode, is extremely succinct:

```
Recur(Left)
Recur(Right)
Return
```

In reality, you also need some way to ensure you don't just fly off the ends of the tree (similar to the check to ensure you don't go off the end of a list). In pseudocode, that looks like:

```
If possible, Recur(Left)
If possible, Recur(Right)
Return
```

Many programmers will, for various reasons both functional and stylistic, elect to place the safety check at the start of a recursive call as follows:

```
If I am not on a node, Return
Otherwise
  Recur(Left)
  Recur(Right)
```
It should be noted that the difference between these two options is that the former places the base case -on- the leaf node, while the latter places the base case -off- the leaf node. Practically, this should hint at when one form or the other is appropriate.

In all cases, the check just has to be performed before you attempt to do anything (either performing work or recurring), otherwise there's a very real possibility that you will attempt an operation on a Null node and your program will crash at run-time.

> Note: Omitting the safety check is a form of logic error; it will compile just fine with the check included or not.

#### PERFORMING WORK DURING TRAVERSAL
Of course, we probably want to do more than just move across the tree. Odds are good we'll want to perform some work. Broadly speaking, there are only a few places you can do things:
```
(YOU CAN DO THINGS HERE)
Recur(Left)
(OR HERE)
Recur(Right)
(OR HERE)
Return
```

In complicated traversals, you can do work at any combination of those three spots. The usual classifications of these formats, however, correspond to three traversal types that are called **Pre-Order**, *In-Order**, and **Post-Order** traversals. Much confusion surrounds the differentiation of these three traversal types, but they correspond to our traversal as follows:

```
(PRE-ORDER WORK IS DONE HERE)
Recur(Left)
(IN-ORDER WORK IS DONE HERE)
Recur(Right)
(POST-ORDER WORK IS DONE HERE)
Return
```
The naming conventions themselves actually refer to this model. In a PRE order traversal, that just means you do work PRE (before) the traversal (left-right recurrence).

In-order means you do work IN-BETWEEN the traversal (left-right recur)

Post-order means you do the work POST (after) the traverse.

Easy peasy!

> Note: In-order has the peculiar quality that, when rendered on a valid binary search tree, it will also happen to print the elements literally in-order. While a happy coincidence, this is not where the name derives.

#### RECURSION IN INDUSTRY
Several companies (or individual engineers) will not accept recursion function calls (that is, self-referential ones) as a valid solution. If you are interviewing and a problem appears to have a recursive solution, **ask your interviewer if using recursion is okay**.

Because your code has to be maintained company-wide, and given that many software teams acknowledge that not everybody understands and can work recursively, recursion may be considered "fancy" programming and be discouraged or forbidden.

In addition, some problem sets are so large that computers may not allow the function call stack to grow large enough to solve them. A recursive solution might work in principle and be elegantly simple to write, but it's entirely possible that for reasons related to technical limitations, it just won't work.

In these cases, recursive code must be refactored to their **iterative** equivalents. That is to say, you must find a way to do the same thing using variables and loops, rather than relying on the call stack to store states. Sometimes this is simple, other times it can be quite difficult and require significant memory resources to accomplish (although not relative to a gigantic call stack; that is -also- memory intensive).

#### TREE TRAVERSAL BY STACK OR QUEUE
It is also possible to load tree nodes to a data container such as a stack or queue. Let's take a look at how this procedure might work:

```
Starting from the root
Enqueue self, then:

While( Queue has elements )
  Pop( Front )
  Attempt to Enqueue( Front.Left )
  Attempt to Enqueue( Front.Right )
```

At a cursory glance, this definitely ensures that all nodes in the tree will eventually pass through and be processed in the queue. But there's something interesting about this particular process.

##### TASK 5
> QUEUE TRAVERSAL
> Draw the first three levels of the following diagram:

![Empty Binary Tree](Img/bintree.jpg)

> Follow the procedure above to number the nodes by the order in which they will be Popped() <br>
> Post your drawing to Discord and write a short summary of your findings with regards to the pattern of Popping() that results. <br>
> <br>
> [Back to checklist](#task-checklist)

#### TREE REPRESENTATION BY ARRAY
Based upon the previous task, you may have noticed something kind of neat about the node numbering. If you translate them directly into an array, it's entirely possible to store a binary tree without ever having to make use of a node class at all! In such a condition, the parent-child relationships are as follows:

```
Left Child == Parent Index * 2
Right Child == (Parent Index * 2) + 1
Parent == Node Index / 2
```

```
+-0-+-1-+-2-+-3-+-4-+-5-+
|   |   |   |   |   |   | ...
+---+---+---+---+---+---+
       
```

##### TASK 6
> Translate our familiar binary search tree to its array equivalent.
> Consider the following:
> 1) If the values are stored naively as an `int[]`, what does it mean for a "node" to be "null"?
> 2) What is the most obvious drawback of this strategy?
> 3) Are there any advantages you can see to representing a tree in this fashion?

##### TASK 7
> Have a Binary Search Tree. Run this.

```
#include <string>
#include <iostream>
#include <sstream>
#include <queue>
#include <cstdlib> // For rand()

class node{
  public:
  bool isDeleted;
  int value;
  node* left;
  node* right;

  // Default constructor
  node() {
    node::tare();
  }

  // Constructor by argument
  node( int arg ) {
    node::tare();
    this->value = arg;
  }

  // The string representation of this node
  std::string toString( ) {
    return std::to_string( this->value );
  }

  private:
  void tare() {
    this->isDeleted = false;
    this->value     = 9999;
    this->left      = nullptr;
    this->right     = nullptr;
  }
};

class bst{
  public:
  node* head;

  // Default constructor
  bst() {
    bst::tare();
  }

  // Constructor by string of arguments
  // Must have at least 1 token to work properly
  bst( std::string arg ) {
    std::stringstream reader;
    reader << arg;
    // Start the tree
    int headVal;
    reader >> headVal;
    this->head = new node( headVal );
    while( !reader.eof() ) {
      int currVal;
      reader >> currVal;
      std::cerr << "Inserting... " << currVal <<std::endl;
      bst::insert( this->head, currVal );
    }
  }

  node* find( node* currNode, int tgtVal ) {
    if( currNode == nullptr ) {
      std::cerr << "Node not found!" << std::endl;
      return nullptr;
    }
    else if( tgtVal > currNode->value) {
      return bst::find( currNode->left, tgtVal );
    }
    else if( tgtVal < currNode->value ) {
      return bst::find( currNode->right, tgtVal );
    }
    else if( tgtVal == currNode->value ) {
      std::cerr << "Node found!" << std::endl;
      return currNode; // We've found it!
    }
  }

  void insert( node* currNode, int tgtVal ) {
    if( currNode->value == tgtVal ) {
      std::cerr << "! " <<std::endl;
    }
    else if( tgtVal > currNode->value ) {
      // Check to the left
      if( currNode->left == nullptr ) {
        // Insert here
        std::cerr << "INSERT(" << tgtVal << ")" << std::endl;
        currNode->left = new node( tgtVal );
      }
      else {
        // Traverse left
        std::cerr << "L(" << currNode->value << ").";
        bst::insert( currNode->left, tgtVal);
      }
    }
    else if( tgtVal < currNode->value ) {
      // Check to the right
      if( currNode->right == nullptr ) {
        // Insert here
        currNode->right = new node( tgtVal );
      }
      else {
      	// Traverse right
        std::cerr << "R(" << currNode->value << ").";
        bst::insert( currNode->right, tgtVal );
      }
    }
  }

  // Draw the tree
  void render() {
    std::queue<node*> currTier;
    std::queue<node*> nextTier;
    nextTier.push( this->head );
    bool notDoneYet = true;
    while( notDoneYet ) {
      // Move the next tier to the current tier
      while( !nextTier.empty() ) {
      	currTier.push( nextTier.front() );
        nextTier.pop();
      }
      
      while( !currTier.empty( ) ) {
        node* currNode = currTier.front();
        currTier.pop();
        std::cout << currNode->value << " ";

        if( currNode-> left == nullptr ) {
          // Do not enqueue anything
        }
        else {
          nextTier.push( currNode->left );
        }

        if( currNode->right == nullptr ) {
          // Do not enqueue anything
        }
        else {
          nextTier.push( currNode->right );
        }
        // Done with this node, so loop up and do the next

      }
      // All children have been loaded to the nextTier
      std::cout << std::endl;

      // But if the next tier has no nodes, we're done
      if( nextTier.empty() ) {
      	notDoneYet = false;
      }

    } // All lines tiers processed
    std::cout << "Done!" << std::endl;
  }

  private:
  void tare() {
    this->head = nullptr;
  }

};

int main( ) {
  bst myTree( "50 25 75" );

  for( int i = 0 ; i < 30 ; i++ ) {

    myTree.insert( myTree.head, std::rand() % 100 );
  }

  myTree.render();

  return 0;
}
```

> Consider the function `bst::render()`. At present, it uses two queues to print each row of the tree. Identify the major shortcoming of the output result and propose a strategy to fix it, improving the legibility of the tree. <br>
> You do not need to implement the fix. <br>
> <br>
> [Back to checklist](#task-checklist)

#### TREE TRAVERSAL BY SAILBOAT
On an exam or an interview, you may (or maybe not) be asked to provide the pre-order, in-order, or post-order sequence of a given tree. This is, of course, a kind of silly question to spring on somebody as a measure of their worth, but the most glaring flaw is that there's a super easy trick to generate the sequence with very little fuss or complication.

It goes like so:

First, imagine that the tree is actually a (very oddly shaped) landmass. Every node is a city, and each has three ports of entry, one on the West, one on the South, and one on the East. The ports are labeled as follows:

![Binary Tree](Img/tree_node_trick.jpg)

In order to get the full report out of the tree, just pretend you are a sailboat tracing your way around this continent. At every city, you enter only at the desired port (either 'Pre', 'In', or 'Post').

![Binary Traverse Trick](Img/tree_traversals.jpg)

That's literally it. If you can remember this, easy points! If not, have fun trying to keep track of the call stack in your head!

##### TASK 8
![ExampleTree](Img/bst.jpg)
> Using our by-now familiar binary tree (or one of your own choosing), write out the traversal results for: <br>
>   A) A Pre-Order Traversal <br>
>   B) An In-Order Traversal <br>
>   C) A Post-Order Traversal <br>
> <br>
> [Back to checklist](#task-checklist)

## PRELAB 1
In many applications, as you type, a dictionary of words is able to identify whether the typed word is known or a typo (that's when you get the squiggly red line underneath). Using what you know so far, how might you go about implementing this sort of functionality if you were programming a word processing application? How would you store and lookup words?
