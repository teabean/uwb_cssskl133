# MAPS AND TRIES

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1 - Real World Recursion](#task-1)
- [ ] [Task 2 - The Grocery Line](#task-2)
- [ ] [Task 3 - The Grocery Line II](#task-3)
- [ ] [Task 4 - The Concert Hall](#task-4)
- [ ] [Task 5 - Queue Traversal](#task-5)
- [ ] [Task 6 - Tree As Array](#task-6)
- [ ] [Task 7 - Binary Tree Example](#task-7)
- [ ] [Task 8 - Tree Traversal By Sailboat](#task-7)
- [ ] [Pre-lab 1](#pre-lab-1)

## ADMINISTRIVIA

### FOLLOWUPS

## TYPOS, AUTO-CORRECT, AND DICTIONARIES
As you type, computers are able to ascertain the correctness of your tokens (words) by comparing them against a dictionary. In lay-speak, a dictionary is just a long listing of words. We could create a datastructure similar to the dictionaries we know and love by using a linked list to daisy chain our known words, one to another.

This isn't great, since to find a word (or add one, if you want to place custom entries in your dictionary) would be an O(N) time operation.

A step up from this, we could create a binary tree of words and use alphabetical ordering to determine the left-right positioning of words in the data structure to get an O(logN) operation for our find() and add(). This is actually quite good!

A step even further than that, we could hash every word and place the entry in a hash table. If we type a word, the computer can then hash the token to see if that's a valid word in O(1) time. This is actually very difficult to beat!

> While the hash table has great lookup and add performance, it is extremely difficult to query the data structure for an ordered list of all words in the dictionary.

All of these are valid strategies to creating a dictionary, which in this case just correlates a token to a true/false value representing its validity. However, none of these data structures seem to have a way of determining if we're en-route to a correct word (the basis of predictive text, wherein your device suggests or recommends what to complete with). In formal-speak, they cannot process prefixes.

While there are only about 30,000 words in the English language, the number of valid prefixes is orders of magnitude higher. At that sort of point, we might actually start to have storage issues. If the dictionary is stored to RAM and every character is a byte, we could be dealing with tens or hundreds of megabytes of storage dedicated just to the dictionary.

To address these shortcomings, dictionaries are often organized into a datastructure known as a [Trie](https://en.wikipedia.org/wiki/Trie) (pronounced "try"). Let's use a simplified trie that only deals with words created using the letters 'a' to 'z' (lowercase, ASCII).

![Trie](Img/trie_node.jpg)

Rather than two children, as in our binary trees, you can have one child for every possible letter of your alphabet, plus one special node used to denote a valid word.

> An alphabet, in CS jargon, are all the valid character states you can deal with. Often this is not the nice, neat, ASCII alphanumerics of A-Z, a-z, 0-9, but can be a truncated set of those or an expanded character set as when dealing with Unicode and foreign languages. In some instances, you may not be dealing with characters at all, but whole data objects.

All pointers begin in a null state. Let's add a word to the trie, starting with the easiest one; the word 'a'.

![Trie Add A](Img/trie_add_a.jpg)

We activate the first letter of the word ('a') by adding another trie node at that child branch, and additionally flag the special, "active word" node connected to 'a'.

To perform a lookup, we could just search for the letter 'a', then check to see if the trie thinks it's a valid word.

Let's now add "ask"

![Trie Add Ask](Img/trie_ask.jpg)

By convention, null nodes will usually be omitted just for tidiness' sake. Let's add a few more words

#### TASK 1
> Using the following trie, identify the words that it contains.

![Trie Words](Img/trie_words.jpg)

> [Back to checklist](#task-checklist)

#### TASK 2
> It is customary to place the valid word flag to the left of letter nodes. Why would that matter? <br>
> <br>
> [Back to checklist](#task-checklist)

#### TASK 3
> Using the sailboat method of tree traversal, perform an in-order traversal on the above trie. What is the result? <br>
> <br>
> [Back to checklist](#task-checklist)

While the time complexity of a trie - O(m) where m is the number of characters in the word - appears to be worse than that of a hash table (O(1)), in practice it is debatable whether performing a hashing operation is in fact faster than tracing a trie. To perform the hash, the entire string must still be handled or processed, and one might be inclined to think that the trie actually gives you the lookup for free since - by the time you finish inspecting the string - you will have already found the terminus on the tree (in fact, you could find that the word is invalid -before- reaching the end of the token). That besides, time complexity is often invoked when handling very large bodies of data ("N" must be large). As English words average only 7 characters in length, an O(7) operation is basically indistinguishable from an O(1) operation.

In any case, where predictive text and lexical dictionaries are concerned, tries are a solid first choice in their implementations.

## HEAPS
Heaps are a data structure which guarantees that the -est (lowest, highest, or some other qualifier) element is always stored at the "top" or "front". If the minimal element is at the top, it's called a Min-Heap, if it's the maximal element, a Max-Heap.

The simplest implementation of this is just as a sorted list (a sorted deque, in fact, can behave as both a Min- and Max-Heap simultaneously).

However, in a linear storage, insertions to the heap will occur in O(N) time, and higher performance is possible by structuring the data as a binary tree (called a binary heap). This differs from BSTs in one important aspect. In a binary max-heap, the parent always has a higher value than its children (a binary min-heap, the reverse, parent is lower in value than its children).

![Max Heap](Img/max_heap.jpg)

You might be inclined to perform add and removal operations on the heap from the top, similar to BSTs. This is a most common error in intuition. These operations must counterintuitively be performed from the bottom-most left or right.

To perform an insertion, append the element to the lowest most corner of the tree, then swap the element up with its parents (bubble it up) until the heap property is met.

![Heap Insertion](Img/heap_insert.jpg)

![Heap Insertion](Img/heap_insert_done.jpg)

You may only delete the root of a heap. To do this, go to the lowest corner of the tree, and move this element to the top of the heap as you perform the deletion. Next, trickle the element down the smaller of the children until the heap property is met.

![Heap Deletion](Img/heap_delete.jpg)

![Heap Deletion](Img/heap_delete_done.jpg)

### TASK 4
> Posit a potential real-worl use for a heap data structure. Share this with your classmates.<br>
> <br>
> [Back to checklist](#task-checklist)

## MAPS AND MAP REPRESENTATION
Data structures can also be used to abstract geographic phenomena. The most familiar example of this has to do with your navigation applications on your mobile device. In this context, your device is able to determine the shortest path between two points, but how does it accomplish this?

### TASK 5
> Given the following graph, representing the streets and intersections around a 2 x 2 city block, determine how many distinct paths there are from 'Start' to 'End'

![Manhattan Navigation](Img/manhattan_start_end.jpg)

> [Back to checklist](#task-checklist)

Suffice to say that real-world navigation is not a problem that can be solved in a timely fashion by exhaustively searching all possible routes. We will not establish a solution this week, but let's look at some of the challenges:

- 1) Geographic distance is not the same as the time or effort to travel (a mile on the freeway may be much easier to travel than a quarter mile by side-road)
- 2) Rates of travel are not the same both ways (in fact, 1-way streets may make travel in a certain direction impossible)
- 3) Backtracking (that is, moving away from your destination temporarily) may be necessary to reach the most efficient route
- 4) The destination may be entirely unreachable from your current position (eg. Hawaii)
- 5) Loops are possible
- 6) Start and end points are arbitrary and the number of possibilities are nearly limitless (N^2 where N are the possible locations to choose from)
- 7) Computers tend to lack a highly performant means of representing graphical relationships like cartography
- 8) In many contexts, computers cannot look at the whole picture and intuit a solution

Despite all this, we do have computationally efficient navigation, so a solution does exist. Let's take our first steps on the road towards that solution.

### COST VERSUS DISTANCE
First, we recognize that despite geography occupying space, we generally are not actually concerned with the physical distance between points.

Next, we must find a means of representing geographic relationships between objects in a way that the computer can understand and work with. There are two main approaches:

### OBJECT-ORIENTED NODES
You can create a node class to represent an intersection or junction. Each node maintains a list of the nodes that they are "adjacent" to (that is, it stores references to adjacent nodes), and correlates the adjacencies with a "cost" to go from 'here' to 'there'. This is easily understandable in the context of real-world navigation, but is not as widely used as the second strategy.

### ADJACENCY MATRIX
The other strategy represents adjacencies as just a simple 2-D Array (a spreadsheet, if you will!) In this schema the columns represent where you are coming from and the rows are where you are headed to (or vice versus, it isn't critical which is which so long as you are consistent). Let's take a look at a small example:

![Adjacency Matrix](Img/adjacency_matrix.jpg)

Often the headers will be stripped and only the body of the matrix retained (it's difficult to mix Strings and other data types in the same matrix, after all).

```
[ [   0 NaN   6   5 NaN ]
  [ NaN   0   3   4 NaN ]
  [   6   3   0 NaN NaN ]
  [   5   4 NaN   0 NaN ]
  [ NaN NaN NaN NaN   0 ] ]
  ```

```
[ Seattle, Bellevue, Mercer Island, Bothell, Bainbridge ]
```

These two bodies of data are sufficient to describe the adjacency matrix and what it represents.

#### TASK 6
What is NaN and why is it used? In lay terms, what does it mean that Bainbridge has an adjacency of NaN, NaN, NaN, NaN, 0?

If you are using `ints` to represent the adjacency cost, describe a strategy to represent non-adjacent elements (that is, those with no direct path between)?

What does the diagonal of '0' values mean?

Note that the values are mirror-imaged across the diagonals. What would it mean if the values on either side of this diagonal differed from one another?

#### TASK 7
Draw the situation described by the adjacency matrix above, labeling each edge between nodes with the appropriate numeric value.

The adjacency matrix has a significant advantage over the object-oriented solution in that all adjacency information is stored to a single data object which can be readily output to .csv (or brough in from .csv), and can be randomly accessed once it is in the program.

We shall revisit adjacency matrices next week.

## COMPRESSION

Zipping a file is a common means of data compression. But what does it mean to compress information? At first blush, this seems somewhat impossible; as soon as you reduce the number of bits used to describe a thing, is there not some data that has been lost which cannot be recovered? As it so happens, there are many, many approaches to data compression (similarly to how data storage is not merely data storage). Every strategy provides particular strengths and trade-offs.

However, let's consider a body of data with the following sequence:

`0000 1010  0000 1010  0000 1010  0000 1010  0000 1010  0000 1010  0000 1010  0000 1010`

If this sequence repeats enough, you might realize that we're wasting a significant amount of storage space on repetitive elements. We can, in fact, **key** a sequence to some other unused bit sequence, then use that bit sequence everywhere the original appears.

`11 == 0000 1010`

`11 11 11 11 11 11 11 11`

This strategy has 2 challenges. First, the key must be maintained in a location and storing that takes space. If the amount of space saved by compression does not exceed the key overhead, application of compression will actually increase the total file size.

> One possible solution is to encode this information as the feature and the number of times it occurs. If that byte represented a dark blue pixel across a large swath of area, it might be easiest to just say "Blue Pixel x 1000" rather than to attempt individual compressions of each feature. This is known as **run-length encoding**. As you might expect, it works better on large, uniform bodies of data than it does on highly variegated bodies where the pattern shifts frequently and the runs are short.

Second, the keys themselves must be discernible as keys when the application reads the bit sequence again to decrompress the data. This is the more daunting of the two issues since compressed data tends to look very similar to actual data (it's all just 1s and 0s). Let us look at a more concrete example. Given the following text:

`abracadabra zillemilloo wipplededop abracadabra wipplededop abracadabra abracadabra abracadabra zillemilloo `

We may generate the following keys:

```
!1 == "abracadabra "
!2 == "zillemilloo "
!3 == "wipplededop "
```

Using these keys, the original sequence may be compressed as:

`!1!2!3!1!3!1!1!1!2`

I can be assured that the compression token ('!') can be recognized because it never appears in the text. If that particular bit sequence shows up, I immediately know that what follows is the key used to decompress. Actual compression keys will behave similarly, but of course will endeavor not to waste precious bits using whole ASCII sequences to describe themselves.

### HUFFMAN CODING
In the 1950s, a fellow by the name of David Huffman noticed that the bit sequences used to describe letters were, themselves, a rich target for data compression. It had been known for some time prior (as early as the 9th century) that the frequency of letters in a given language, statistically, was not even (in fact, the apocryphal tale goes that this is how Mary, the Queen of Scots, lost her head in the 1580s when her cipher was broken through the use of [character frequency analysis](https://en.wikipedia.org/wiki/Frequency_analysis)). Huffman realized that if the frequency of letters in a body of text were not equal, the number of bits used to represent them could reflect this.

Using the idea of keying above, Huffman added a detail in that every character could be keyed, but that the shortest, unambiguous key would be matched to the most frequent letter, with successively longer keys associated to more infrequent characters. More than that, he developed a process (an algorithm!) to translate a body of text into a Huffman Tree (a data structure!) for this very purpose!

And now, [Tom Scott](https://www.youtube.com/watch?v=JsTptu56GM8)!

### LOSSY IMAGE COMPRESSION
Video streams are an increasingly ubiquitous service in our society for both entertainment and work purposes. They are also heavyweight in terms of the bandwidth required, and companies which deal in video transfer (Netflix, Zoom, YouTube, Disney+, etc., etc., as well as the ISPs who have to carry and serve this traffic) have a vested interest in video compression in order to improve the quality of their services and to reduce the costs of providing that service (both in hosting the video files and transferring them to end users).

Because the use case of videos allows for loss, a number of compression strategies can be used that would not be appropriate for other data types. These strategies are called 'lossy', because they are associated with an irreversible degredation of the data. However, this degredation can yield significant performance benefits.

Let's take a quick, mile-high view of some of the unique features of video that we can leverage:
- 1. Often, swaths of an image will remain unchanged from frame to frame
- 2. If images are divided into small parts, chances are good that visually similar parts have been seen previously.
- 3. The changes (diffs) from a prior frame almost always takes up less space than a standalone frame

These observations, and the fact that we can be assured that video compression is being applied to the media we view, are reflected in the various artifacts you've seen for many years.

## PRELAB 1
Internet routers behave very much like the junctions of roads. Each router maintains connections to adjacent routers and monitors the state of traffic from itself to its neighbors (these are, effectively, the time-cost to transfer traffic from one router to another). You may take somewhat for granted that when you request to connect to a site like 'youtube.com', the routers will be able to direct your traffic to the appropriate IP address of record.

However, that poses something of a problem for the server (which is really just a computer in one of Google's closets). Not only is it geographically distant from most of its users (and therefore slow), but the sheer volume of requests for tens of thousands of video streams would be a significant challenge to serve through even the bests of network connections. You can only fit so much data through a wire.

Hypothesize a system by which a service such as YouTube.com might provide rapid video streams to a multitude of users. As a hint, consider how a torrent or other Peer-to-Peer file sharing service operates.

