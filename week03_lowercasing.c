#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void whisper( char* original, char* copy );

int main( int argc, char* argv[] ) {
  char myString[ 13 ] = "HeLlO WoRlD!";
  printf( "%s \n", myString );

  char myLowercaseString[ 13 ];

  whisper( myString, myLowercaseString );

  printf( "%s \n", myLowercaseString );

}

void whisper( char* original, char* copy ) {
  int count = strlen( original );
  printf( "Count: %d \n", count );
  for( int i = 0 ; i < count ; i++ ) {
    char copyChar = original[ i ];
    if( copyChar >= 65 && copyChar <= 90 ) {
    	copyChar += 32;
    }
    copy[ i ] = copyChar;
  }
  copy[ count ] = '\0';
}
