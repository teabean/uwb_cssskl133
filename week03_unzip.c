#include <stdio.h>
#include <stdlib.h>

int unzip( int* a, int* b, int* c, int n );
void printArray( int* tgtArray, int count );

int main( int argc, char* argv[] ) {
  int a[] = { 1, 2, 3, 4, 5, 6 }; // Assumes array is divisible by 2
  int dataElementCount = sizeof( a ) / sizeof( int ); // Size of 'a' is total bytes
  int unzipCount = dataElementCount / 2;

  printf( "Data  element count == %d \n", dataElementCount );
  printf( "Unzip element count == %d \n", unzipCount );

  int b[ unzipCount ];
  int c[ unzipCount ];
  unzip( a, b, c, unzipCount );
  printArray( a, dataElementCount );
  printArray( b, unzipCount );
  printArray( c, unzipCount );
}

int unzip( int* a, int* b, int* c, int n ) {
  for( int i = 0 ; i < n ; i++ ) {
  	b[i] = a[ ( 2 * i ) ];
  	c[i] = a[ ( 2 * i ) + 1 ];
  }
}

void printArray( int* tgtArray, int count ) {
  for( int i = 0 ; i < count ; i++ ) {
  	printf( "%d ", tgtArray[ i ] );
  }
  printf( "\n" );
}
