---
Document frontmatter
CREATOR:     Tim Lum
SUBJECT:     Linked lists
DESCRIPTION: A laboratory assignment specification for the University of Washington Bothell CSSSKL133 course 
FORMAT:      Markdown
LANGUAGE:    English, Markdown, C++
LICENSE:     BSD 3-Clause
CONTACT:     twhlum@gmail.com
TAGS:        data structures, algorithms, linked list, nodes, C++
---

# LINKED LISTS

## OVERVIEW

### THIS DOCUMENT...
Describes a laboratory assignment specification covering the topic of Linked Lists (LL) Data Structure (DS), implemented in C++.

### OBJECTIVES
- Describe the LL DS
  - Node
  - Singly-Linked
  - Doubly-Linked
  - Time Complexity
- Introduce associated LL Algorithms (Algos)
  - Traverse
  - Count
  - Insert
  - Delete
  - Copy
- Introduce bugs and corner cases
  - Intersection (Convergence)
  - Broken chain
  - Loops / Cycles
- Tips and Tricks
  - Node Destructor ("!")
  - Recursive Deletion
  - Lazy Deletion


### TASKS
Tasks for this assignment:

- [ ] DECLARE AND INSTANTIATE A NODE
- [ ] DECLARE AND INSTANTIATE A LINKED LIST
- [ ] ADD NODES TO THE LINKED LIST
- [ ] TRAVERSE ACROSS THE LINKED LIST
- [ ] INSERT A NODE AT A POSITION
- [ ] DELETE A NODE WITH A GIVEN VALUE
- [ ] COPY THE LINKED LIST
- [ ] DELETE THE LINKED LIST

### TABLE OF CONTENTS
[[_TOC_]]

## CONTEXT

### THE RULES

1. Nullptr may be assigned to a Node*
2. Nullptr may be observed and compared against safely (it is a number)
3. Attempting to dereference (unpack) Nullptr will result in an error (because the address it points to is invalid)

### CONVENTIONS

- `HEAD` - The start or 'handle' of a linked list.
- `CURR` - The current node reflecting one's position in the list.
- `NEXT` - The next node in sequence relative to `CURR`.
- `PREV` - The previous node in sequence relative to `CURR`.
- `TAIL` - The end of the linked list.
- `PTR` - Suffix denoting that a variable is a pointer.

```mermaid
graph LR
HEAD(HEAD) --> N1(...) --> PREV(PREV) --> CURR(CURR) --> NEXT(NEXT) --> N2(...) --> TAIL(TAIL)
```

## DESCRIPTION

### WHAT IS A LINKED LIST?
(From [Wikipedia](https://en.wikipedia.org/wiki/Linked_list))

_In computer science, a linked list is a linear collection of data elements whose order is not given by their physical placement in memory. Instead, each element points to the next. It is a data structure consisting of a collection of nodes which together represent a sequence. In its most basic form, each node contains: data, and a reference (in other words, a link) to the next node in the sequence._

![LinkedList](Img/LinkedList_Overview.svg "Diagramatic representation of a linked list")

### THE NODE

### SINGLY-LINKED

Where the node contains only a reference to the next node in sequence, the list is described as being "singly-linked". In a singly-linked list, examination of the list (traversal) may only be performed from start to finish. There is often no feasible way to perform any sort of "reverse" action.

### DOUBLY-LINKED

Doubly-linked lists, in contrast, contain a pointer to the previous (prev) node as well as the next. Thanks to this, operations may be efficiently performed that require backing up, such as "find a node, then modify a prior one based on that information". In addition, the tail of the list may be connected to the head, allowing rapid access to the tail.

### TIME COMPLEXITY

Singly linked lists with no embellishments will have the following performance characteristics in the worst case:
  - `Find()` - O(N) - Find an element in the list
  - `Insert()` - O(N) - Insert an element to the tail or other appropriate location of the list
  - `Delete()` - O(N) - Delete an element from the list
  - `Count()` - O(N) - Count the number of elements in the list

With the exception of operations performed at the `head`, all operations must examine every element in the list.

## IMPLEMENTATION

### THE TOOLS
The Node class (minimally):
```cpp
class Node {
  Node* nextPtr;
  int value;
}
```

The Linked List class (minimally):
```cpp
class LinkedList {
  Node head;
  
  LinkedList() {
    this->head = new Node();
  }
}
```

The Driver:
```cpp
int main( ) {
  LinkedList myList = new LinkedList();
}
```

## TASKS

Using the following (optional) script:

compile.sh
```
rm lab.out
g++ --std=c++11 laboratory.cpp -o lab.out
./lab.out
```

### DECLARE AND INSTANTIATE A NODE

Driver:
```cpp
#include <iostream>
#include <iomanip>

struct Node {
  // TODO: Declare and implement the Node class
};

int main( int argc, char* argv[] ) {
  Node* n_ptr = new Node( 123 );
  std::cout << n_ptr->value << std::endl;
  std::cout << std::boolalpha << (n_ptr->next == nullptr) << std::endl;
} // Closing main()
```

Expected output:
```
123
true
```

### DECLARE AND INSTANTIATE A LINKED LIST

Driver:
```cpp
#include <iostream>

struct Node {
  int value;
  Node* next;
  Node( int arg ) {
    this->value = arg;
  }
};

struct LinkedList {
  // TODO: Declare and implement the Linked List class
  Node head;
};

int main( int argc, char* argv[] ) {
  LinkedList L = LinkedList();
  L.head->value = 123;
  std::cout << L.head->value << std::endl;
} // Closing main()
```

Expected output:
```
123
```

### TRAVERSE ACROSS THE LINKED LIST
To traverse a list, we first set a pointer to a Node (`curr`). We then assign the list `head` to `curr`.

In order to "move" down the list, we reassign `curr->next` to `curr`.

In order to stop, this movement should be wrapped in some sort of test to identify the correct condition to pause:

```cpp
while( some condition is not met ) {
  curr = curr->next;
}
```

### ADD NODES TO THE TAIL OF THE LINKED LIST
As a baseline approach, we may traverse to the end, then insert the new node at that position. Under this approach, we need only take care to check that the **next** node is not null. If we wait until the `curr` node is null, we will in fact have gone off the end of the list and be unable to perform the appending action.

This requires O(N) time to complete; if the list is a billion elements long, we must traverse a billion elements to perform this operation.

To refine this approach, the class itself should maintain a pointer to the last node in the list. Doing so will allow the correct reference to be acquired in O(1) time. 

### INSERT A NODE AT A POSITION
Similar to adding to the tail, insertion requires just two slight adjustments.
  - 1. The test for the traversal does not look for `nullptr`, but rather for an appropriate condition to be met
  - 2. The test should be performed ahead of the insertion location. If `curr` is identified **after**, it will be too late to insert.
  - 3. `curr->next` must be held to a `temp` variable while the insertion is being made (this is similar to `swap()`)

```cpp
while( insertion condition is not met on curr-next ) {
  curr = curr->next;
}
Node* temp         = curr->next;
curr->next         = &nodeToInsert;
nodeToInsert->next = temp;
```

### DELETE A NODE WITH A GIVEN VALUE

### COPY THE LINKED LIST

### DELETE THE LINKED LIST

## BUGS + CORNER CASES

### BROKEN CHAIN

```mermaid
graph LR;
  A --> B --> C;
  D --> E --> F;
```

If, for whatever reason, the linked list is ever 'broken' (that is, the 'next' reference of a node is redirected inappropriately), all subsequent nodes in the list will be lost. Once detached, this lost memory is essentially unrecoverable and becomes known as a 'memory leak' - allocated memory that cannot be used, accessed, or deleted.

### LOOPS / CYCLES

On the odd chance that one of the nodes points back to a prior node in the LL, a structure called a 'cycle' will occur. In this, you may note that our handy iterative loop of `while( node->next != nullptr )` will in fact just go on forever.

```mermaid
graph LR;
  A --> B --> C --> D --> E --> C;
```

But how do you tell the difference between just a very long, similar looking list (think DNA sequences) and a true cycle? The solution to this problem is colloquially called the 'Tortoise and Hare' algorithm and formally as '[Floyd's Algorithm](https://en.wikipedia.org/wiki/Cycle_detection#Floyd's_Tortoise_and_Hare)'. In it, two pointers traverse the list at differing rates. As a result, the faster pointer will eventually either reach the end or 'lap' the slower pointer. Should the two pointers ever indicate the same node, you can be well assured that a loop exists.

### INTERSECTION / CONVERGENCE

```mermaid
graph LR;
  A --> B --> C --> D --> E;
  F --> G --> C;
```

### RECURSIVE DELETION

A recursive callstack may be used to delete elements from the end of the Linked List. In testing, this will probably work just fine and yields code that is succinct and elegant.

```cpp
void delete( Node* tgt ) {
  if( tgt->next != nullptr ) {
    delete( tgt->next );
  }

  delete tgt;
}
```

However, doing so introduces a bug. Namely, the maximal linked list length that may be deleted in this fashion is equal to the OS-enforced depth of the call stack that you may use. If this is exceeded, you will create a stack overflow and your program will terminate in an uncontrolled fashion.

This bug is particularly insidious in that it will likely not appear during testing or evaluation, as the length of test lists are likely to be significantly smaller than the maximum callstack depth.

Because of this, an iterative solution which deletes from the front is preferred to a recursive solution that deletes from the back.

## TIPS + TRICKS

### NODE DESTRUCTOR MESSAGE

Appending a `cerr` or `cout` message to the destructor may assist in debugging which nodes are deleted when and where (or, as is the case when hunting memory leaks... which nodes are -not- being deleted).

### LAZY DELETION

"Lazy Deletion" is a trick that can be used in most data structures. In it, the node definition is modified to include an `isDeleted` boolean (or similar signal). When the data structure is queried as to whether an object is "in the set" or not, it will look for the value as normal and check for both the presence of the node in question as well as its deletion flag.

Why the added complexity?

In situations where memory is not a concern, lazy deletion often requires less time to perform than the restructuring and background processes needed to properly deallocate memory. Often, a performance improvement can be gleaned by leaving the element in situ, but flagging it as being "deleted". While the benefits are debateable in a Linked List situation, the advantages of lazy deletion become more apparent in data structures where removal may demand a reshuffling of a large number of data elements.

Lazy deletion also has the ancillary benefit that if your program is being evaluated for memory leaks, there is a chance that improperly deleted resources will not show in the final memory report since those resources are technically still in use and not lost or detached.

## REFERENCES

[1]
```json
@startjson
{
  "VALUE": "123",
  "NEXT":
  {
    "VALUE": "456",
    "NEXT":
    {
      "VALUE": "789",
      "NEXT": [ "!" ]
    }
  }
}
@endjson
```

[2] If applicable
