# QUICKSORT

![Card deck](Img/cards.jpg)

Given an unsorted (well, we don't know if it's sorted or not) array of data from index 0 to 'N':

| 5 | 1 | 3 | 2 | 4 | 6 | 7 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |

### STEP 0 - CALL QUICKSORT()
We begin with a call to sort the array, passing the requisite data. This information is:
- The array location
- The low bound to sort
- The high bound to sort

Initially, the low and high bounds will correspond exactly with the `[0]`th index and the `[N]`th index. However, subsequent calls to `quicksort()` will be performed upon subsets of this body, hence the need to define the sort boundaries every time after.

### STEP 1 - PICK A 'PIVOT' VALUE
This is one of the indexes in the data we're sorting. It is spiritually the same idea as picking a random card from a deck. Ideally, the pivot should be the median value, but this is of course difficult to do without examining all the cards first (and that wastes time/operations). To be efficient, we must perform this selection blind.

Originally (back in the 1960s), we took the leftmost index, but that had unfortunate time side-effects in the worst case (reverse-sorted lists). A workable solution is to simply pick one element in the range at random.

> A significant amount of discussion has been leveled at how best to choose the pivot (card). The current best practice is something called 'Median-of-Three', which involves taking the low bound card, the high bound card, and the card in the int-divided middle, then using the middle card of these three selections.

| 7 | 1 | 3 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   |   |   |   | ^ |

In my case, using either the left-most index or median-of-three, I just pick `index [6]` (5), but any value can be expected to work comparably well for a shuffled list.

### STEP 2 - CALL PARTITION()
If we assume that a call to `partition()` will perform the action we want in an efficient fashion, then we need only call partition and pass it the appropriate starting data. This data is:
- The array to sort
- The low index bound for partitioning
- The high index bound for partitioning

In return, we will receive the index location of a (now sorted) partition value.

> Sorting in this particular case only means that all values to the left are smaller than the partition, and all values to the right are larger. No guarantee is made about the orderings of those data subsets.

### STEP 3 - BUT WHAT DOES PARTITION() DO?
`Partition()` operates by pair-wise comparison from the front and rear of the array. It selects first the left-most and right-most values, compares them, and - if they are out of order - swaps them if necessary. The end result of the swap is that the smaller value will be placed to the right, and the larger to the left.

We must first establish two counters, traditionally 'i' and 'j'

> Yes, 'i' begins at index -1. Don't worry, the first time we need to use 'i', we'll increment it by 1.

|   | 7 | 1 | 3 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| i | j |   |   |   |   |   | ^ |

So remembering that 5 happens to be our pivot value, we're going to use the counters `i` and `j` to shuffle up the array in the following fashion:

- 1. Check `[j]` against our pivot value.
  - If it is larger, do nothing
  - If it is smaller, increment `i`, then swap `[j]` with `[i]`
- 2. Increment `j`, and repeat until all numbers have been checked.
- 3. At that point, increment `i` by 1 and swap with the pivot's index.

##### Iteration 1:

|   | 7 | 1 | 3 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| i | j |   |   |   |   |   | ^ |

"7 is greater than 5, so do nothing"

##### Iteration 2:

|   | 7 | 1 | 3 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   | i | j |   |   |   |   | ^ |

"1 is less than 5, so increment i"

|   | 1 | 7 | 3 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   | i | j |   |   |   |   | ^ |

"Then swap!"

##### Iteration 3:

|   | 1 | 7 | 3 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   | i | j |   |   |   | ^ |

"3 is less than 5, so increment i"

|   | 1 | 3 | 7 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   | i | j |   |   |   | ^ |

"Then swap!"

##### Iteration 4:

|   | 1 | 3 | 7 | 2 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   | i | j |   |   | ^ |

"2 is less than 5, so increment i"

|   | 1 | 3 | 2 | 7 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   | i | j |   |   | ^ |

"Then swap!"

##### Iteration 5:

|   | 1 | 3 | 2 | 7 | 4 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   |   | i | j |   | ^ |

"4 is less than 5, so increment i"

|   | 1 | 3 | 2 | 4 | 7 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   |   | i | j |   | ^ |

"Then swap!"

##### Iteration 6:

|   | 1 | 3 | 2 | 4 | 7 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   |   | i |   | j | ^ |

"6 is greater than 5, so do nothing"

> At this point, we've pretty much "stacked up" all the values to one side or the other. Notice how everything smaller than the pivot is to the right, and everything larger is towards the left (excepting the pivot, which is just hanging way the heck out there).

##### End behavior:

Finally, we need to put the pivot in the correct spot.

|   | 1 | 3 | 2 | 4 | 7 | 6 | 5 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   |   |   | i |   | ^ |

"Finally, increment i..."

|   | 1 | 3 | 2 | 4 | 5 | 6 | 7 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   |   |   | i |   | ^ |

"And swap with the pivot!"

- 4. Finally, return the new index position of the pivot.

| 4 |
| :--- |

Return (4)

### STEP 4 - SO... THAT RETURN VALUE...
We now have a partially sorted array and the pivot (5) is in the correct location (everything to the left is smaller, everything to the right is larger).

We also know the location of the pivot based off the return value from `partition()` (index 4).

| 1 | 3 | 2 | 4 | 5 | 6 | 7 |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|   |   |   |   | X |   |   |

So what do we do with this information?

We make two more calls to `quicksort()`! Only this time we're going to quicksort everything to the left of the pivot with one call (`quicksort( 0, (4) - 1 )`), and everything to the right with another (`quicksort( (4)+1, (size-1))`)

These calls to quicksort() will perform the partition action on their respective subsets, calling quicksort() again and again until everything is sorted.

### STEP 5 - BUT HOW DO WE KNOW WHEN TO STOP?
As is, the above strategy will sort the array, but then it will continue to operate and fall down a recursive loop. We need a means by which to halt this process and signal the stack of function calls we've created (literally called the "call stack") to collapse once its work is completed.

We'll address exiting recursion next week, but for now, congrats, you've quicksorted() an array!

