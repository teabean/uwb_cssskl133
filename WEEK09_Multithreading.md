# MULTITHREADING

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1](#task-1)
- [ ] [Task 2](#task-2)
- [ ] [Task 3](#task-3)
- [ ] [Task 4](#task-4)
- [ ] [Task 5](#task-5)
- [ ] [Task 6](#task-6)
- [ ] [Task 7](#task-7)
- [ ] [Task 8](#task-7)
- [ ] [Pre-lab 1](#pre-lab-1)

## FOLLOW-UPS

### DISTRIBUTED RESOURCES
The solution to the many-clients/single-server problem is to create many, mostly identical servers. We can place these servers geographically close to where users are, thus reducing the time to service user requests and the number of requests that have to be pushed to the main server.

### PEER-TO-PEER (P2P)
P2P designs take this to the extreme by having users act as servers. If a resource is requested in a P2P network, other users will check to see if they can serve that request and - if they can - then the answer will come from other user(s) rather than a centralized server. This is more common with file-sharing where a single resource has a relatively static state and is large enough that it can be reasonably handled in smaller segments.

#### TASK 1
> Despite the historic copyright legal cases that shut down P2P services such as Kazaa and Napster in the late 90s, P2P remains a useful tool for content distribution when the content is: <br>
> A) Static <br>
> B) Widely copied <br>
> C) Divideable into pieces <br>
> D) Not time-critical <br>
>  <br>
> Posit some examples of Peer-to-peer content distribution. <br>
> <br>
> [Back to checklist](#task-checklist)

### CONTENT DELIVERY NETWORKS (CDNS)
Content Delivery Networks (or CDNs) maintain a collection of servers across geographic regions (like North America, Asia, Europe, etc.). They sell their services to content distributors like YouTube, Giphy, Vimeo, or Imgur (among others) and act as co-hosts for content. When new material needs to be pushed to end users, this data will be sent across the CDNs where it will be duplicated geographically close to users who need it.

#### TASK 2
> If you were a CDN responsible for getting data from your client (YouTube) out to all their end users, what would your priorities and/or objectives be? <br>
> <br>
> How might you go about achieving these objectives? <br>
> <br>
> [Back to checklist](#task-checklist)

### CACHING
CDNs are extremely large examples of caches. The idea of a cache is that we can create smaller storage locations with faster access time (but probably higher cost to operate) between a data source and where data is consumed. Where the cache is able to serve a data request, the trip back to the primary source (or a cache level above) can be spared and the speed to serve thereby reduced.

#### TASK 3
> What is your web browser's TEMP folder and why does it exist? <br>
> <br>
> [Back to checklist](#task-checklist)

There are a large number of caches sitting between your CPU (the data consumption point) and any given resource on your computer or on a network. Most modern CPUs are served directly by about 3 caches (labelled the L1 to L3) that sits between the CPU socket and RAM (meaning most data requests have to fail all cache requests - called a 'cache miss') before RAM will be accessed.

The optimization of caches, then, is actually a critical area of study in any context where expedient data access is a priority. As one might expect, a large body of research and a number of algorithms have been developed to try and address this.

The question that cache algorithms attempt to access may be stated thus:

"What information should we load to a cache?"

The answer is actually surprisingly straightforward: You should load the data that will result in the fewest cache misses.

Similar to the game of Tetris, if you knew the sequence of requests a priori, you could merely analyze it and load the appropriate data when you needed it. This theoretical situation almost never exists, however; in reality, the cache doesn't know what request is going to be coming up next.

#### CACHE ALGORITHMS
So we have **cache algorithms** which is basically a fancy way of determining what information a cache retains and what information it drop. Generally, these algorithms seek to:

1) Prioritize the most accessed information to the lowest (fastest) levels of caching
2) Not duplicate information at a higher level of caching

In effect, however, they are policy implementations and are just strategies used to populate (and depopulate) the caches.

A brief overview of common strategies may be found at the [Wiki article](https://en.wikipedia.org/wiki/Cache_replacement_policies) on the topic.

In practice, it is likely that if you ever have to optimize a cache or other cache-like structure, you will need to instrument your caches to determine the cache-miss ratio, then ascertain the best policy experimentally.

#### TASK 4
> You have a workshop. Why is it a really bad idea to always return your tools and projects to their proper storage locations after every use or operation? <br>
> <br>
> Propose an alternative. <br>
> <br>
> [Back to checklist](#task-checklist)

#### CACHE CONCURRENCY
As mentioned previously, coordination of the contents in the caches remains an outstanding challenge in computer science. This isn't merely an issue of duplication of data (which would be inefficient, but not otherwise dangerous), but consider the following situation:

The cache requests the value of a variable. It receives it (say, '1234').
Some other piece of software logic increments the variable value to '1235'.

Immediately, we can see that if we have no means of propagating this change to the caches, the system will retain multiple values for the same variable and the usefulness of the cache architecture plummets. One of the first controls for this came in the 1980s, when caches were allowed to observe the state of data busses. Should a write operation occur at a location which the cache held, the caches could observe this action and invalidate out-of-date data in a process called **bus snooping**.

> Bus snooping isn't really an option for internet caches like the CDNs. Generally, content in the CDNs is purely top-down, with data generated at a certified publishing source being distributed back down to all the nodes of the network. This poses a challenge where content must be driven back to the server, as in comments or the counting of likes. In these situations, it is often not possible to drive the changes directly to the network nodes, and the performance of accepting changes and pushing those alterations back to all endpoints remains a challenge.

Algorithms for cache concurrency are also called **coherence protocols**, and generally boil down to the question of "What should the caches do in the event of a write operation?"

## SEQUENTIAL CODE EXECUTION
Up to this point, the instructions you have written are sequential. That is, they execute one line at a time and the execution pointer only exists in one location of the program at any given moment. Unfortunately, the fact that we have a name for this implies that we needed a distinction from something else that did not follow this paradigm.

### NON-SEQUENTIAL CODE EXECUTION
That 'something else' is non-sequential code execution (also often called **parallel programming** or **asynchronous programming**). The many names for this refer to the same principle: What if we could run our code in separate instances that moved independent of one another?

Your first reaction is probably, "Why would we want to open that can of worms?"

### THE ADVANTAGES
If only from a usage standpoint, it should be readily apparent that users want non-concurrency. We do not want to use a system that must always complete one task, then the next, and so on; we actually want a system that does (or appears to, anyhow) many tasks simultaneously. Your operating system is a great example of this, where you are able to listen to music while you browse the internet with Netflix or a game running in the background. Our entire compute experience is predicated on having multiple applications (that is, computer programs) executing in parallel.

But even individual applications can benefit tremendously from non-concurrence. Consider, if your program begins to open a large file (like a video), wouldn't it be nice if you could just start watching before the file was completely brought into RAM? Or what about viewing a page before it has finished loading all the resources needed to fully render?

Beyond that, many problems which are on the borderline of being feasibly solved by computers can often be brute forced just by using more processors in parallel with one another. If it takes 100 years to simulate a protein molecule, why not solve that problem in a few hours by spooling up 300 thousand processors to work on it in parallel? This is, in point of fact, exactly what "supercomputers" are doing.

![Supercomputer](Img/supercomputer.jpg)

Unsurprisingly, there's a whole family of algorithms that were developed to aid in the subdivision of large problem sets called [parallel algorithms](https://en.wikipedia.org/wiki/Parallel_algorithm). We will not be covering these in this course, but if your future career takes you into the world of distributed computing, these are the sorts of strategies you will likely need to learn and implement to take advantage of parallel computation.

Every execution pointer in this system is referred to as a **thread**. You might be inclined to say that each processor core handles one thread but, while it's true that each core can only operate on one thread at any given moment, the number of threads each core is associated with may be significantly higher.

> More threads does not necessarily guarantee higher performance. There is a significant logistics overhead to changing thread contexts and having too many threads open can result in the CPU spending more time performing context switches than actual work. Not unlike people and multitasking, in fact; often it is most efficient to run a task to completion or a reasonable stopping point before ALT+TABing to the next thing.

### THE PROBLEM
Having established that parallel computing can be advantageous, let us look at the source of a major problem when designing parallel systems.

### ATOMICITY
Remember the compiler? That whole 'press compile' thing? What is it actually doing? At a high level, it's converting your C++ (or Java, or whatever compiled language you're writing in) into... something else. That something else is a thing called [Assembly Code](https://en.wikipedia.org/wiki/X86_instruction_listings), and these are the instructions which are recognizable by the processor. Compared to the language you write in (C++), the available instructions are relatively few, and they look quite simple. You may even recognize a few in there, like 'INC' and 'DEC' which increment and decrement by 1.

But... increment or decrement _what_ by 1? And where do we place the results?

If you look down the available assembly instructions, you'll note that there is no 'read' or 'write', only the loading of data registers. To increment something, the processor must load data from the register, perform the increment, then reload that value back to the register. In point of fact, `myVar++` actually requires three operations that are performed together as a single unit to give the appearance of a single, more complicated procedure. This bundling is known as **atomicity**.

### RACE CONDITIONS
But what happens if parallel threads of execution attempt to increment the same variable at approximately the same time? All the threads perform their loads from the register...

#### TASK 5
> Then what? What problem did we just create and what does this appear similar to? <br>
> <br>
> [Back to checklist](#task-checklist)

When substantive behavior is dependent upon the sequence of execution, we have what is called a **race condition**.

> Almost always, this is referred to as a software bug of the above sort, in which multiple threads appear to "race" towards resources, often to deleterious effect. Although this is the common understanding, the correct definition is above. Not all race conditions are bugs.

#### LOCKS AND MUTEXES
To combat this, resources or operations that are susceptible to race conditions (called **critical sections**) are often associated with a **lock** or **mutex** (short for MUTual EXclusion). C++ actually includes this data structure natively, and it works in the following fashion:

```cpp
#include <iostream>
#include <thread>
#include <mutex>

std::mutex mtx; // Note: As a global variable, all threads share the same Mutex (there is only one)

void print_block (int n, char c) {
  // critical section (exclusive access to std::cout signaled by locking mutex)
  mtx.lock();
  for (int i=0; i<n; ++i) { std::cout << c; }
  std::cout << '\n';
  mtx.unlock();
}

int main ()
{
  std::thread th1 (print_block,50,'*');
  std::thread th2 (print_block,50,'$');

  th1.join();
  th2.join();

  return 0;
}
```

#### TASK 6
> Run the above code with and without the mutex. Compare results and posit why they appear the way they do. <br>
> <br>
> [Back to checklist](#task-checklist)

> One of the most common uses of massively parallel, threaded programming is in internet servers. In order to respond to the many inbound requests, servers often dedicate a thread to each request/response cycle

## DIJKSTRA'S ALGORITHM
Developed in 1956, Djikstra's algorithm establishes a means to identify the shortest path between two graph nodes. There are a few caveats (the graph must be finite, the points connected, costs cannot be negative, etc... basically reasonable assumptions to make about a graph), but it forms the basis of many graph navigation applications including map navigation and internet routing.

The question it sought to address was, from a given start point, how can we be sure that the path selected is definitely the shortest (lowest cost) route?

Consider that even if two nodes are adjacent, there is no guarantee that an exploration in the opposite direction wouldn't be faster. In addition, an exhaustive search of all possible routes would be computationally infeasible for even moderately sized graphs.

### THE NEAREST NODE
The first observation we may make is that of the adjacent nodes, the nearest node cannot be reached by any other route in a more expedient fashion.

All the other ones? We're not so sure about those (a faster route might exist through the nearest one we just identified, for instance).

### THE NEXT NEAREST NODE
The second observation that was less obvious than the first is that, of the nodes we can now reach, the -next nearest- node is definitely reached in the most expedient fashion.

### REACHING THE END
By repeating this process of node-by-node exploration of the nearest nodes first, the algorithm guarantees that by the time you reach the target destination, you will have found the shortest route there _provided you made proper notes of your path during the exploration_. If you didn't notate that, you'll only have the shortest distance to the end, but not necessarily the path to get there.

https://youtu.be/pSqmAO-m7Lk?t=230

### OPTIMIZATIONS TO DIJKSTRA'S
In an actual geographic exploration where edge costs are similar to geographic distance, you may note the major shortcoming of Dijkstra's Algorithm. While it guarantees correctness, it must explore the space in roughly an expanding circle about the start point, this to establish a path that basically looks like a radius. Obviously, this does not appear to be ideal (and it's not).

https://www.youtube.com/watch?v=g024lzsknDo

However, if you begin the exploration from both the start and the end, an interesting guarantee arises where the two explorations meet.

#### TASK 7
> Assuming you perform Dijkstra's from both the start and the end, if both executions identify a node as being the next nearest, what does that mean? <br>
> <br>
> [Back to checklist](#task-checklist)

There are, of course, other optimizations that have been found and applied to Dijkstra's algorithm since it was developed in the 1950s, but the principle of node-edge exploration still finds widespread applicability in the world today.

## COMPUTATIONAL COMPLEXITY THEORY
https://en.wikipedia.org/wiki/Computational_complexity_theory

As it so happens, the many problems we have identified have actually been recorded, catalogued, and organized into a body of data known as **computational complexity theory**. The basic idea of this categorization is to give computer scientists a rapid means of establishing the most efficient solution they can hope to achieve for any given problem they face. The problem space with which we are most familiar (things like sorting a set of numbers or searching for a value) tend to occupy the narrowest, innermost problem domain.

These problems are surrounded by computationally infeasible ones, and the outermost rings reach some philosophically interesting (but not terribly useful) problem spaces (like "problems that can't be stated")

It should be noted, however, that the position of problems within these spaces may or may not be proven. They reflect our best current best knowledge on the topic, and there's no reason an algorithm or paradigm change in the future might not upset these positions, moving intractable problems into the space of solvable problems.

## QUANTUM COMPUTE AND QUANTUM ALGORITHMS
Some buzz has been established around the topic of quantum computation and how it will revolutionize or "break" old computing. Quantum computers definitely have capabilities that traditional computers do not, but the way and degree in which it will shake up computation is fairly well understood in theory.

Because traditional complexity spaces are created by known bounds and limitations, Quantum Complexity Spaces may be established by noting where such limits no longer apply. Unfortunately for tech enthusiasm, the Bounded Quantum problem space (BQP space) yields only moderate theoretical gains into the known problem space.

It'll be a great tool to have and, yes, certain elements of computation will change in response, but it doesn't appear to be the tremendous upset that some people are hoping for.

## PRELAB 1
For review next week, please furnish any questions or clarifications you would like with regards to this quarter's topics and problems. Code problems are welcome.
